package cl.jaa.projects.payment.presentation.selectpayment.reducer

import cl.jaa.projects.payment.factory.PaymentFactory.generateListCardItem
import cl.jaa.projects.payment.presentation.selectpayment.SelectPaymentResult
import cl.jaa.projects.payment.presentation.selectpayment.SelectPaymentUiState
import org.junit.Assert.assertTrue
import org.junit.Test

class DisplayPaymentMethodsUiStateReducerTest {

    private val reducer = DisplayPaymentMethodsUiStateReducer()

    @Test
    fun `given DisplayPaymentMethodsUiState with Success, when reduce, then return DisplayPaymentMethodsUiState`() {
        val currentState = SelectPaymentUiState.DisplayPaymentMethodsUiState(generateListCardItem())
        val result = SelectPaymentResult.SaveSelectedPaymentMethodResult.Success

        val newState = reducer.reduce(currentState, result)

        assertTrue(newState is SelectPaymentUiState.DisplayPaymentMethodsUiState)
    }
}