package cl.jaa.projects.payment.data.remote.model

import cl.jaa.projects.payment.data.remote.model.Constants.ID
import cl.jaa.projects.payment.data.remote.model.Constants.NAME
import cl.jaa.projects.payment.data.remote.model.Constants.THUMBNAIL
import com.google.gson.annotations.SerializedName

data class RemoteCardIssuersItem(
    @SerializedName(ID) val id: String?,
    @SerializedName(NAME) val name: String?,
    @SerializedName(THUMBNAIL) val thumbnail: String?
)