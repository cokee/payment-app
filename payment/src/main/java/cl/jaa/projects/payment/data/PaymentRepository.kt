package cl.jaa.projects.payment.data

import cl.jaa.projects.payment.data.cache.CachePaymentParams
import cl.jaa.projects.payment.data.remote.model.RemoteCardIssuersItem
import cl.jaa.projects.payment.data.remote.model.RemotePaymentItem
import cl.jaa.projects.payment.data.source.PaymentCache
import cl.jaa.projects.payment.data.source.PaymentRemote
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class PaymentRepository @Inject constructor(
    private val remote: PaymentRemote,
    private val cache: PaymentCache,
) {
    private val gson: Gson = GsonBuilder().create()

    fun getPaymentMethods() = flow {
        emit(remote.getRemotePaymentMethods())
    }

    fun getCardIssuers() = flow {
        val params = getPaymentParams().first()
        emit(remote.getCardIssuers(params.card?.id.toString()))
    }

    fun getInstallments() = flow {
        val params = getPaymentParams().first()
        emit(
            remote.getInstallments(
                paymentMethodId = params.card?.id.toString(),
                issuerId = params.bank?.id.toString(),
                amount = params.amount.toString()
            )
        )
    }

    suspend fun saveAmount(amount: String) {
        cache.saveAmount(amount)
    }

    suspend fun saveCard(card: RemotePaymentItem) {
        val jsonCard = gson.toJson(card)
        cache.saveCard(jsonCard)
    }

    suspend fun saveBank(bank: RemoteCardIssuersItem) {
        val jsonCard = gson.toJson(bank)
        cache.saveBank(jsonCard)
    }

    suspend fun saveInstallments(installments: String) {
        cache.saveInstallments(installments)
    }

    fun getPaymentParams() = flow {

        val amount = cache.getAmount().first()
        val card = gson.fromJson(cache.getCard().first(), RemotePaymentItem::class.java)
        val bank = gson.fromJson(cache.getBank().first(), RemoteCardIssuersItem::class.java)
        val installments = cache.getInstallments().first()

        emit(
            CachePaymentParams(
                amount = amount,
                card = card,
                bank = bank,
                installments = installments
            )
        )
    }
}