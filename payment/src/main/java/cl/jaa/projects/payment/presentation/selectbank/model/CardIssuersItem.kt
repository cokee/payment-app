package cl.jaa.projects.payment.presentation.selectbank.model

data class CardIssuersItem(
    val id: String,
    val name: String,
    val thumbnail: String
)