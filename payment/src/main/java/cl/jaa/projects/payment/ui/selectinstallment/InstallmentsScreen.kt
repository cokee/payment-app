package cl.jaa.projects.payment.ui.selectinstallment

import androidx.compose.material.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import androidx.compose.ui.res.stringResource
import cl.jaa.projects.payment.R
import cl.jaa.projects.payment.presentation.selectinstallment.SelectInstallmentUiState
import cl.jaa.projects.payment.ui.commons.*

@Composable
fun InstallmentScreen(
    uiState: State<SelectInstallmentUiState>,
    selectCardEvent: (item: SpinnerItem) -> Unit,
    receiptEvent: () -> Unit
) {
    Scaffold {
        SelectInstallmentContent(
            uiState,
            selectCardEvent,
            receiptEvent
        )
    }
}

@Composable
private fun SelectInstallmentContent(
    state: State<SelectInstallmentUiState>,
    selectedItemEvent: (item: SpinnerItem) -> Unit,
    receiptEvent: () -> Unit
) {
    when (val uiState = state.value) {
        SelectInstallmentUiState.DefaultUiState -> DefaultContent()
        is SelectInstallmentUiState.DisplayInstallmentsUiState -> SpinnerSelection(
            selectedItemEvent = selectedItemEvent,
            spinnerItems = uiState.installments,
            headerLabel = stringResource(id = R.string.select_installment)
        )
        SelectInstallmentUiState.ErrorUiState -> ErrorComponent()
        SelectInstallmentUiState.LoadingUiState -> LoadingComponent()
        is SelectInstallmentUiState.DisplayPaymentParamsUiState -> DisplayReceipt(
            uiState.paymentParams,
            receiptEvent = receiptEvent
        )
    }
}