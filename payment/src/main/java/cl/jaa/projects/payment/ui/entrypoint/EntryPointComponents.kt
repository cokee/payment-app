package cl.jaa.projects.payment.ui.entrypoint

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Button
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.sp
import cl.jaa.projects.payment.R

@Composable
fun DisplayInputAmountSection(nextStepEvent: (amount: String) -> Unit) {

    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .padding(dimensionResource(id = R.dimen.padding_8))
            .fillMaxSize(),
        verticalArrangement = Arrangement.Center
    ) {
        Text(
            text = stringResource(id = R.string.welcome),
            modifier = Modifier.fillMaxWidth(),
            textAlign = TextAlign.Center,
            fontSize = 30.sp,
            fontWeight = FontWeight.Bold
        )
        var value by remember { mutableStateOf("") }
        var visibility by remember { mutableStateOf(false) }
        var isError by rememberSaveable { mutableStateOf(false) }

        OutlinedTextField(
            keyboardOptions = KeyboardOptions.Default.copy(
                keyboardType = KeyboardType.Number,
                imeAction = ImeAction.Done
            ),
            keyboardActions = KeyboardActions(onDone = {
                if (value.isEmpty() || !isNumber(value)) {
                    isError = true
                } else {
                    nextStepEvent(value)
                }
            }),
            value = value,
            onValueChange = {
                if (it.length <= 6) {
                    value = it
                }
            },
            label = { Text(stringResource(id = R.string.insert_an_amount)) },
            maxLines = 1,
            textStyle = TextStyle(color = Color.Blue, fontWeight = FontWeight.Bold),
            modifier = Modifier.padding(dimensionResource(id = R.dimen.padding_8)),
            isError = isError
        )
        if (isError) {
            Text(
                text = stringResource(id = R.string.incorrect_amount),
                color = Color.Red
            )
        }
        Button(onClick = {
            if (value.isEmpty() || !isNumber(value)) {
                isError = true
            } else {
                nextStepEvent(value)
            }
        }, modifier = Modifier.fillMaxWidth()) {
            Text(text = stringResource(id = R.string.to_continue))
        }
    }
}

fun isNumber(value: String): Boolean {
    return try {
        value.toInt()
        true
    } catch (e: NumberFormatException) {
        false
    }
}

@Preview
@Composable
fun previewDisplayInputAmountSection() {
    DisplayInputAmountSection({})
}