package cl.jaa.projects.payment.factory

import cl.jaa.projects.payment.common.DefaultValues
import cl.jaa.projects.payment.data.remote.model.RemoteCardIssuersItem
import cl.jaa.projects.payment.presentation.selectbank.model.CardIssuersItem
import cl.jaa.projects.payment.ui.commons.CardItem

object PaymentFactory {

    fun generateListCardIssuerItem(count: Int = 2) = (0..count).map {
        generateCardIssuersItem()
    }


    fun generateCardIssuersItem() = CardIssuersItem(
        id = DefaultValues.BLANK_STRING,
        name = DefaultValues.BLANK_STRING,
        thumbnail = DefaultValues.BLANK_STRING
    )

    fun generateListCardItem(count: Int = 2) = (0..count).map {
        generateCardItem()
    }

    fun generateCardItem() = CardItem(
        id = DefaultValues.BLANK_STRING,
        title = DefaultValues.BLANK_STRING,
        thumbnail = DefaultValues.BLANK_STRING
    )
}