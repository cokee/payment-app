package cl.jaa.projects.payment.factory

import cl.jaa.projects.payment.common.DefaultValues
import cl.jaa.projects.payment.data.cache.CachePaymentParams
import cl.jaa.projects.payment.factory.RemotePaymentFactory.generateCardIssuersItem
import cl.jaa.projects.payment.factory.RemotePaymentFactory.generateRemotePaymentItem

object CachePaymentFactory {

    fun generateCachePaymentParams() = CachePaymentParams(
        amount = Math.random().toString(),
        installments = DefaultValues.EMPTY_STRING,
        bank = generateCardIssuersItem(),
        card = generateRemotePaymentItem()
    )
}