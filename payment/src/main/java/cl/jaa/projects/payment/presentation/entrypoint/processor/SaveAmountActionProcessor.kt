package cl.jaa.projects.payment.presentation.entrypoint.processor

import cl.jaa.projects.payment.data.PaymentRepository
import cl.jaa.projects.payment.presentation.entrypoint.EntryPointAction
import cl.jaa.projects.payment.presentation.entrypoint.EntryPointActionProcessor
import cl.jaa.projects.payment.presentation.entrypoint.EntryPointResult
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class SaveAmountActionProcessor @Inject constructor(
    private val repository: PaymentRepository
) : EntryPointActionProcessor<EntryPointAction, EntryPointResult> {
    override suspend fun process(action: EntryPointAction): Flow<EntryPointResult> = flow {
        val act = action as EntryPointAction.SaveAmountAction
        repository.saveAmount(act.amount)
        emit(EntryPointResult.SaveAmountResult.Success)
    }
}