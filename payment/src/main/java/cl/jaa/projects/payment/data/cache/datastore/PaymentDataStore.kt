package cl.jaa.projects.payment.data.cache.datastore

import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import cl.jaa.projects.payment.data.cache.datastore.config.PaymentDataStoreFactory
import cl.jaa.projects.payment.data.cache.datastore.config.PaymentDataStoreFactory.Companion.AMOUNT
import cl.jaa.projects.payment.data.cache.datastore.config.PaymentDataStoreFactory.Companion.BANK
import cl.jaa.projects.payment.data.cache.datastore.config.PaymentDataStoreFactory.Companion.CARD
import cl.jaa.projects.payment.data.cache.datastore.config.PaymentDataStoreFactory.Companion.INSTALLMENTS
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class PaymentDataStore @Inject constructor(
    private val factory: PaymentDataStoreFactory
) {
    private val amountKey = stringPreferencesKey(AMOUNT)
    private val cardKey = stringPreferencesKey(CARD)
    private val bankKey = stringPreferencesKey(BANK)
    private val installmentsKey = stringPreferencesKey(INSTALLMENTS)

    suspend fun saveAmount(amount: String) {
        with(factory) {
            getDataStore.edit { preferences ->
                preferences[amountKey] = amount
            }
        }
    }

    suspend fun saveCard(card: String) {
        with(factory) {
            getDataStore.edit { preferences ->
                preferences[cardKey] = card
            }
        }
    }

    suspend fun saveBank(bank: String) {
        with(factory) {
            getDataStore.edit { preferences ->
                preferences[bankKey] = bank
            }
        }
    }

    suspend fun saveInstallments(installments: String) {
        with(factory) {
            getDataStore.edit { preferences ->
                preferences[installmentsKey] = installments
            }
        }
    }

    fun getAmount(): Flow<String> = with(factory) {
        getDataStore.data.map { preferences ->
            preferences[amountKey] ?: ""
        }
    }

    fun getCard(): Flow<String> = with(factory) {
        getDataStore.data.map { preferences ->
            preferences[cardKey] ?: ""
        }
    }

    fun getBank(): Flow<String> = with(factory) {
        getDataStore.data.map { preferences ->
            preferences[bankKey] ?: ""
        }
    }

    fun getInstallments(): Flow<String> = with(factory) {
        getDataStore.data.map { preferences ->
            preferences[installmentsKey] ?: ""
        }
    }
}
