package cl.jaa.projects.payment.common

object DefaultValues {
    const val EMPTY_STRING = ""
    const val BLANK_STRING = " "
    const val ZERO_INT = 0
}