package cl.jaa.projects.payment.presentation.selectpayment.reducer

import cl.jaa.projects.core.mvi.UnsupportedReduceException
import cl.jaa.projects.payment.presentation.selectpayment.SelectPaymentResult
import cl.jaa.projects.payment.presentation.selectpayment.SelectPaymentResult.GetPaymentMethodsResult.Failure
import cl.jaa.projects.payment.presentation.selectpayment.SelectPaymentResult.GetPaymentMethodsResult.Success
import cl.jaa.projects.payment.presentation.selectpayment.SelectPaymentUiReducer
import cl.jaa.projects.payment.presentation.selectpayment.SelectPaymentUiState
import cl.jaa.projects.payment.presentation.selectpayment.SelectPaymentUiState.DisplayPaymentMethodsUiState
import cl.jaa.projects.payment.presentation.selectpayment.SelectPaymentUiState.ErrorUiState
import javax.inject.Inject

class LoadingUiStateReducer @Inject constructor() : SelectPaymentUiReducer {
    override fun reduce(
        currentState: SelectPaymentUiState,
        result: SelectPaymentResult
    ): SelectPaymentUiState {
        return when (result) {
            is Success -> DisplayPaymentMethodsUiState(
                result.paymentMethods
            )
            is Failure -> ErrorUiState
            else -> throw UnsupportedReduceException(currentState, result)
        }
    }
}