package cl.jaa.projects.payment.data.remote

import cl.jaa.projects.payment.data.remote.model.RemoteCardIssuersItem
import cl.jaa.projects.payment.data.remote.model.RemoteInstallmentsItem
import cl.jaa.projects.payment.data.remote.model.RemotePayerCost
import cl.jaa.projects.payment.data.remote.model.RemotePaymentItem
import cl.jaa.projects.payment.data.remote.retrofit.PaymentWebService
import cl.jaa.projects.payment.data.source.PaymentRemote
import javax.inject.Inject

class PaymentRemoteImpl @Inject constructor(private val webService: PaymentWebService) :
    PaymentRemote {
    private val publicKey = "444a9ef5-8a6b-429f-abdf-587639155d88"
    override suspend fun getRemotePaymentMethods(): List<RemotePaymentItem> =
        webService.getPaymentMethods(publicKey)

    override suspend fun getCardIssuers(paymentMethodId: String): List<RemoteCardIssuersItem> =
        webService.getCardIssuers(publicKey, paymentMethodId)

    override suspend fun getInstallments(
        paymentMethodId: String,
        issuerId: String,
        amount: String
    ): List<RemotePayerCost> =
        webService.getInstallments(
            publicKey = publicKey,
            amount = amount,
            issuerId = issuerId,
            paymentMethodId = paymentMethodId
        )[0].payerCosts
            ?: listOf()

}