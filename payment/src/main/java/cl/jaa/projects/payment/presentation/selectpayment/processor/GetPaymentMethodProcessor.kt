package cl.jaa.projects.payment.presentation.selectpayment.processor

import cl.jaa.projects.payment.data.PaymentRepository
import cl.jaa.projects.payment.presentation.selectpayment.SelectPaymentAction
import cl.jaa.projects.payment.presentation.selectpayment.SelectPaymentActionProcessor
import cl.jaa.projects.payment.presentation.selectpayment.SelectPaymentResult
import cl.jaa.projects.payment.presentation.selectpayment.mapper.PaymentMethodsMapper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import javax.inject.Inject

class GetPaymentMethodProcessor @Inject constructor(
    private val repository: PaymentRepository,
    private val mapper: PaymentMethodsMapper
) : SelectPaymentActionProcessor<SelectPaymentAction, SelectPaymentResult> {
    override suspend fun process(action: SelectPaymentAction): Flow<SelectPaymentResult> =
        repository.getPaymentMethods().map { paymentMethods ->
            SelectPaymentResult.GetPaymentMethodsResult.Success(
                with(mapper) { paymentMethods.toPresentation() }
            ) as SelectPaymentResult.GetPaymentMethodsResult
        }.onStart {
            emit(SelectPaymentResult.GetPaymentMethodsResult.InProgress)
        }.catch {
            emit(SelectPaymentResult.GetPaymentMethodsResult.Failure)
        }.flowOn(Dispatchers.IO)

}