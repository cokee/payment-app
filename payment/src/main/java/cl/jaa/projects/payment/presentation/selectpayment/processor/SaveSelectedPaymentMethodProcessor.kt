package cl.jaa.projects.payment.presentation.selectpayment.processor

import cl.jaa.projects.payment.data.PaymentRepository
import cl.jaa.projects.payment.presentation.selectpayment.SelectPaymentAction
import cl.jaa.projects.payment.presentation.selectpayment.SelectPaymentActionProcessor
import cl.jaa.projects.payment.presentation.selectpayment.SelectPaymentResult
import cl.jaa.projects.payment.presentation.selectpayment.SelectPaymentResult.SaveSelectedPaymentMethodResult.*
import cl.jaa.projects.payment.presentation.selectpayment.mapper.PaymentMethodsMapper
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class SaveSelectedPaymentMethodProcessor @Inject constructor(
    private val repository: PaymentRepository,
    private val paymentMethodsMapper: PaymentMethodsMapper
) : SelectPaymentActionProcessor<SelectPaymentAction, SelectPaymentResult> {
    override suspend fun process(action: SelectPaymentAction): Flow<SelectPaymentResult> = flow {
        val actionResult = action as SelectPaymentAction.SaveSelectedPaymentMethodAction
        repository.saveCard(with(paymentMethodsMapper) { actionResult.selectedPaymentMethod.toRemote() })
        emit(Success)
    }
}