package cl.jaa.projects.payment.presentation.selectbank

import javax.inject.Inject
import javax.inject.Provider

class SelectBankReducer @Inject constructor(
    private val reducers: Map<Class<out SelectBankUiState>, @JvmSuppressWildcards Provider<SelectBankUiStateReducer>>
) :
    SelectBankUiStateReducer {
    override fun reduce(
        currentState: SelectBankUiState,
        result: SelectBankResult
    ): SelectBankUiState {
        return reducers[currentState::class.java]?.get()?.reduce(currentState, result)
            ?: SelectBankUiState.DefaultUiState
    }
}
