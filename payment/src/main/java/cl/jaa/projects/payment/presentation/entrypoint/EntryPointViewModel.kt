package cl.jaa.projects.payment.presentation.entrypoint

import cl.jaa.projects.payment.presentation.entrypoint.EntryPointAction.SaveAmountAction
import cl.jaa.projects.payment.ui.commons.MviViewModel
import cl.jaa.projects.payment.ui.navigation.NavActions
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@FlowPreview
class EntryPointViewModel @Inject constructor(
    reducer: EntryPointReducer,
    processor: EntryPointProcessor
) : MviViewModel<EntryPointUiStateReducer,
        EntryPointUiState,
        EntryPointResult,
        EntryPointAction,
        EntryPointProcessor,
        EntryPointUIntent>(
    reducer,
    processor,
    EntryPointUiState.DefaultUiState
) {
    var navActions: NavActions? = null

    override fun EntryPointUIntent.toAction(): EntryPointAction {
        return when (this) {
            is EntryPointUIntent.ContinueNextStepUIntent -> SaveAmountAction(amount)
        }
    }

    override fun Flow<EntryPointResult>.checkResultToNavigation(): Flow<EntryPointResult> =
        onEach { result ->
            when (result) {
                EntryPointResult.SaveAmountResult.Success -> {
                    navActions?.payment?.invoke()
                }
                else -> return@onEach
            }
        }
}

