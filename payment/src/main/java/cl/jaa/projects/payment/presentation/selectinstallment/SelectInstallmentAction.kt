package cl.jaa.projects.payment.presentation.selectinstallment

import cl.jaa.projects.core.mvi.MviAction
import cl.jaa.projects.payment.ui.commons.SpinnerItem


sealed class SelectInstallmentAction : MviAction {
    object GetInstallmentsAction : SelectInstallmentAction()
    data class SaveSelectedInstallmentAction(val item: SpinnerItem): SelectInstallmentAction()
}
