package cl.jaa.projects.payment.data.remote.model


import com.google.gson.annotations.SerializedName

data class RemotePaymentItem(
    @SerializedName(Constants.ID) val id: String?,
    @SerializedName(Constants.NAME) val name: String?,
    @SerializedName(Constants.THUMBNAIL) val thumbnail: String?
)