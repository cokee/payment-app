package cl.jaa.projects.payment.presentation.entrypoint

import cl.jaa.projects.core.mvi.MviUserIntent

sealed class EntryPointUIntent : MviUserIntent {
    data class ContinueNextStepUIntent(val amount: String) : EntryPointUIntent()
}
