package cl.jaa.projects.payment.data.remote

import cl.jaa.projects.payment.factory.RemotePaymentFactory.generateListCardIssuerItem
import cl.jaa.projects.payment.factory.RemotePaymentFactory.generateListRemoteInstallmentsItem
import cl.jaa.projects.payment.factory.RemotePaymentFactory.generateRemotePaymentMethods
import cl.jaa.projects.payment.data.remote.model.RemoteCardIssuersItem
import cl.jaa.projects.payment.data.remote.model.RemoteInstallmentsItem
import cl.jaa.projects.payment.data.remote.model.RemotePaymentItem
import cl.jaa.projects.payment.data.remote.retrofit.PaymentWebService
import io.mockk.coEvery
import io.mockk.mockk
import junit.framework.Assert.assertTrue
import kotlinx.coroutines.runBlocking
import org.junit.Test

class PaymentRemoteImplTest {

    private val api = mockk<PaymentWebService>()

    private val remote = PaymentRemoteImpl(api)

    @Test
    fun `given publicKey, when getPaymentMethods, then return data`() = runBlocking {
        val remotePaymentMethods = generateRemotePaymentMethods(3)
        stubGetPaymentMethods(remotePaymentMethods)

        val result = remote.getRemotePaymentMethods()

        assertTrue(result == remotePaymentMethods)

    }

    @Test
    fun `given paymentMethod, when getCardIssuers, then return data`() = runBlocking {
        val remoteListCardIssuerItem = generateListCardIssuerItem()

        stubGetCardIssuers(remoteListCardIssuerItem)

        val result = remote.getCardIssuers(paymentMethodId = "visa")

        assertTrue(result == remoteListCardIssuerItem)

    }

    @Test
    fun `when getInstallments, then return data`() = runBlocking {
        val remoteListCardIssuerItem = generateListRemoteInstallmentsItem()

        stubGetInstallments(remoteListCardIssuerItem)

        val result = remote.getInstallments(paymentMethodId = "visa", amount = "3000", issuerId = "299")

        assertTrue(result == remoteListCardIssuerItem[0].payerCosts)

    }


    private fun stubGetPaymentMethods(paymentMethods: List<RemotePaymentItem>) {
        coEvery { api.getPaymentMethods(any()) } returns paymentMethods
    }

    private fun stubGetCardIssuers(
        paymentMethods: List<RemoteCardIssuersItem>
    ) {
        coEvery { api.getCardIssuers(any(), any()) } returns paymentMethods
    }

    private fun stubGetInstallments(
        paymentMethods: List<RemoteInstallmentsItem>
    ) {
        coEvery { api.getInstallments(any(), any(), any(), any()) } returns paymentMethods
    }
}
