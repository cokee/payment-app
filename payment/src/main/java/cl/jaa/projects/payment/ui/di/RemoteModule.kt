package cl.jaa.projects.payment.ui.di

import cl.jaa.projects.payment.data.remote.PaymentRemoteImpl
import cl.jaa.projects.payment.data.remote.retrofit.PaymentWebService
import cl.jaa.projects.payment.data.source.PaymentRemote
import dagger.Binds
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@Module
abstract class RemoteModule {

    companion object {
        private val baseUrl = "https://api.mercadopago.com/"

        @Provides
        fun provideWebServiceRetrofit(): PaymentWebService {

            val interceptor = HttpLoggingInterceptor().apply {
                setLevel(HttpLoggingInterceptor.Level.BODY)
            }
            val client = OkHttpClient.Builder().addInterceptor(interceptor).build()

            return Retrofit
                .Builder()
                .baseUrl(baseUrl)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(PaymentWebService::class.java)
        }
    }

    @Binds
    abstract fun bindRemote(remote: PaymentRemoteImpl): PaymentRemote
}