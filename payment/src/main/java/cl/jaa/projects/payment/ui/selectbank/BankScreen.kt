package cl.jaa.projects.payment.ui.selectbank

import androidx.compose.material.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import androidx.compose.ui.res.stringResource
import cl.jaa.projects.payment.R
import cl.jaa.projects.payment.presentation.selectbank.SelectBankUiState
import cl.jaa.projects.payment.ui.commons.*

@Composable
fun BankScreen(
    uiState: State<SelectBankUiState>,
    selectCardEvent: (cardItem: CardItem) -> Unit
) {
    Scaffold {
        SelectBankContent(
            uiState,
            selectCardEvent
        )
    }
}

@Composable
private fun SelectBankContent(
    state: State<SelectBankUiState>,
    selectCardEvent: (cardItem: CardItem) -> Unit
) {
    when (val uiState = state.value) {
        SelectBankUiState.DefaultUiState -> DefaultContent()
        is SelectBankUiState.DisplayBanksUiState -> DisplaySelectableCardItems(
            cardItems = uiState.banks,
            selectCardEvent = selectCardEvent,
            headerLabel = stringResource(id = R.string.select_bank)
        )
        SelectBankUiState.ErrorUiState -> ErrorComponent()
        SelectBankUiState.LoadingUiState -> LoadingComponent()
    }
}
