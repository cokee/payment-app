package cl.jaa.projects.payment.ui.di

import cl.jaa.projects.payment.presentation.entrypoint.EntryPointUiState
import cl.jaa.projects.payment.presentation.entrypoint.EntryPointUiStateReducer
import cl.jaa.projects.payment.presentation.entrypoint.reducer.DefaultUiStateReducer
import cl.jaa.projects.payment.presentation.entrypoint.reducer.ErrorUiStateReducer
import cl.jaa.projects.payment.presentation.entrypoint.reducer.NavigateToSelectPaymentUiStateReducer
import dagger.Binds
import dagger.MapKey
import dagger.Module
import dagger.multibindings.IntoMap
import kotlin.reflect.KClass

@Module
abstract class EntryPointUiStateReducerModule {
    @Binds
    @IntoMap
    @EntryPointUiStateReducerKey(EntryPointUiState.DefaultUiState::class)
    abstract fun bindDefaultUiStateReducer(reducer: DefaultUiStateReducer): EntryPointUiStateReducer

    @Binds
    @IntoMap
    @EntryPointUiStateReducerKey(EntryPointUiState.ErrorUiState::class)
    abstract fun bindErrorUiStateReducer(reducer: ErrorUiStateReducer): EntryPointUiStateReducer

}

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER, AnnotationTarget.PROPERTY_SETTER)
@MapKey
annotation class EntryPointUiStateReducerKey(val kclass: KClass<out EntryPointUiState>)