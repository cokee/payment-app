package cl.jaa.projects.payment.presentation.selectbank

import cl.jaa.projects.core.mvi.MviReducer

interface SelectBankUiStateReducer: MviReducer<SelectBankUiState, SelectBankResult>