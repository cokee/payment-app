package cl.jaa.projects.core.mvi

import kotlinx.coroutines.flow.Flow

interface MviPresentation<TUIntent : MviUserIntent, TUiState: MviUiState> {
    fun processUserIntentsAndObserveUiStates(userIntents: Flow<TUIntent>): Flow<TUiState>
}