package cl.jaa.projects.payment.data.remote.model

object Constants {
    const val ID = "id"
    const val NAME = "name"
    const val THUMBNAIL = "thumbnail"
    const val INSTALLMENTS = "installments"
    const val RECOMMENDED_MESSAGE = "recommended_message"
    const val PAYER_COSTS = "payer_costs"
}
