package cl.jaa.projects.payment.presentation.entrypoint.reducer

import cl.jaa.projects.payment.presentation.entrypoint.EntryPointResult
import cl.jaa.projects.payment.presentation.entrypoint.EntryPointUiState
import cl.jaa.projects.payment.presentation.entrypoint.EntryPointUiStateReducer
import javax.inject.Inject

class DefaultUiStateReducer @Inject constructor() : EntryPointUiStateReducer {
    override fun reduce(
        currentState: EntryPointUiState,
        result: EntryPointResult
    ): EntryPointUiState {
        return when (result) {
            EntryPointResult.SaveAmountResult.Success -> currentState
            EntryPointResult.NoneResult -> EntryPointUiState.ErrorUiState
        }
    }
}