package cl.jaa.projects.payment.presentation.selectpayment

import cl.jaa.projects.core.mvi.MviReducer

interface SelectPaymentUiReducer :MviReducer<SelectPaymentUiState,SelectPaymentResult>