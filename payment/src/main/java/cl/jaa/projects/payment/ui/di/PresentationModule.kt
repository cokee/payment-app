package cl.jaa.projects.payment.ui.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import cl.jaa.projects.payment.presentation.entrypoint.EntryPointViewModel
import cl.jaa.projects.payment.presentation.selectbank.SelectBankViewModel
import cl.jaa.projects.payment.presentation.selectinstallment.SelectInstallmentViewModel
import cl.jaa.projects.payment.presentation.selectpayment.SelectPaymentViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap


@Module(
    includes = [
        EntryPointActionProcessorModule::class,
        EntryPointUiStateReducerModule::class,
        SelectPaymentActionProcessorModule::class,
        SelectPaymentUiStateReducerModule::class,
        SelectBankActionProcessorModule::class,
        SelectBankUiStateReducerModule::class,
        SelectInstallmentActionProcessorModule::class,
        SelectInstallmentUiStateReducerModule::class,
    ]
)
internal abstract class PresentationModule {

    @Binds
    @IntoMap
    @ViewModelKey(EntryPointViewModel::class)
    abstract fun bindEntryPointViewModel(viewModel: EntryPointViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SelectPaymentViewModel::class)
    abstract fun bindPaymentViewModel(viewModel: SelectPaymentViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SelectBankViewModel::class)
    abstract fun bindSelectBankViewModel(viewModel: SelectBankViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SelectInstallmentViewModel::class)
    abstract fun bindSelectInstallmentViewModel(viewModel: SelectInstallmentViewModel): ViewModel

    @Binds
    abstract fun bindsViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}