package cl.jaa.projects.payment.presentation.selectpayment

import cl.jaa.projects.core.mvi.MviProcessor
import javax.inject.Inject
import javax.inject.Provider

class SelectPaymentProcessor @Inject constructor(
    processors: Map<Class<out SelectPaymentAction>, @JvmSuppressWildcards Provider<SelectPaymentActionProcessor<SelectPaymentAction, SelectPaymentResult>>>
):MviProcessor<SelectPaymentAction,SelectPaymentResult,SelectPaymentActionProcessor<SelectPaymentAction,SelectPaymentResult>>(
    processors = processors,
    defaultResult = SelectPaymentResult.NoneResult
)
