package cl.jaa.projects.payment.presentation.selectpayment

import cl.jaa.projects.payment.presentation.selectpayment.SelectPaymentUiState.DefaultUiState
import javax.inject.Inject
import javax.inject.Provider

class SelectPaymentReducer @Inject constructor(
    private val reducers: Map<Class<out SelectPaymentUiState>, @JvmSuppressWildcards Provider<SelectPaymentUiReducer>>
) : SelectPaymentUiReducer {
    override fun reduce(
        currentState: SelectPaymentUiState,
        result: SelectPaymentResult
    ): SelectPaymentUiState {
        return reducers[currentState::class.java]?.get()?.reduce(currentState, result)
            ?: DefaultUiState
    }
}
