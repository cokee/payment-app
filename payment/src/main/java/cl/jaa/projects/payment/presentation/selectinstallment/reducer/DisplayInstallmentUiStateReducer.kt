package cl.jaa.projects.payment.presentation.selectinstallment.reducer

import cl.jaa.projects.core.mvi.UnsupportedReduceException
import cl.jaa.projects.payment.presentation.selectbank.SelectBankResult
import cl.jaa.projects.payment.presentation.selectbank.SelectBankUiState
import cl.jaa.projects.payment.presentation.selectbank.SelectBankUiStateReducer
import cl.jaa.projects.payment.presentation.selectinstallment.SelectInstallmentResult
import cl.jaa.projects.payment.presentation.selectinstallment.SelectInstallmentResult.*
import cl.jaa.projects.payment.presentation.selectinstallment.SelectInstallmentUiState
import cl.jaa.projects.payment.presentation.selectinstallment.SelectInstallmentUiStateReducer
import javax.inject.Inject

class DisplayInstallmentUiStateReducer @Inject constructor() : SelectInstallmentUiStateReducer {
    override fun reduce(
        currentState: SelectInstallmentUiState,
        result: SelectInstallmentResult
    ): SelectInstallmentUiState {
        return when (result) {
            is SaveInstallmentSelectedResult.Success -> SelectInstallmentUiState.DisplayPaymentParamsUiState(
                result.paymentParams
            )
            else -> throw UnsupportedReduceException(currentState, result)
        }
    }
}