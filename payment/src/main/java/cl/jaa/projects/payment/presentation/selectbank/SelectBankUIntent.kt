package cl.jaa.projects.payment.presentation.selectbank

import cl.jaa.projects.core.mvi.MviUserIntent
import cl.jaa.projects.payment.ui.commons.CardItem

sealed class SelectBankUIntent : MviUserIntent {
    object SeeBanksUIntent : SelectBankUIntent()
    data class SelectedBankUIntent(val selectedBank: CardItem) : SelectBankUIntent()
    object RetrySeeBanksUIntent : SelectBankUIntent()
}