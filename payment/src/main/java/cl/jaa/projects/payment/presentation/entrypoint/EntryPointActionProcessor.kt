package cl.jaa.projects.payment.presentation.entrypoint

import cl.jaa.projects.core.mvi.MviActionProcessor

interface EntryPointActionProcessor<A: EntryPointAction, R: EntryPointResult>: MviActionProcessor<A, R>