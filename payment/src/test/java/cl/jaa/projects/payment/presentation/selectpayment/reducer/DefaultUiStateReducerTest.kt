package cl.jaa.projects.payment.presentation.selectpayment.reducer

import cl.jaa.projects.payment.presentation.selectpayment.SelectPaymentResult
import cl.jaa.projects.payment.presentation.selectpayment.SelectPaymentUiState
import org.junit.Assert.*
import org.junit.Test

class DefaultUiStateReducerTest {

    private val reducer = DefaultUiStateReducer()

    @Test
    fun `given DefaultUiState with InProgress, when reduce, then return LoadingUiState`() {
        val currentState = SelectPaymentUiState.DefaultUiState
        val result = SelectPaymentResult.GetPaymentMethodsResult.InProgress

        val newState = reducer.reduce(currentState, result)

        assertTrue(newState is SelectPaymentUiState.LoadingUiState)
    }
}