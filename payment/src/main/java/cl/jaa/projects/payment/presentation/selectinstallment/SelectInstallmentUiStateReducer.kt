package cl.jaa.projects.payment.presentation.selectinstallment

import cl.jaa.projects.core.mvi.MviReducer

interface SelectInstallmentUiStateReducer: MviReducer<SelectInstallmentUiState, SelectInstallmentResult>