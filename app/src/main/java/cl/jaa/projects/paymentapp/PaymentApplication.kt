package cl.jaa.projects.paymentapp

import android.app.Application
import cl.jaa.projects.payment.ui.di.PaymentComponentProvider
import cl.jaa.projects.paymentapp.di.AppComponent
import cl.jaa.projects.paymentapp.di.DaggerAppComponent
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview

@FlowPreview
@ExperimentalCoroutinesApi
class PaymentApplication : Application(), PaymentComponentProvider {
    private val appComponent: AppComponent by lazy {
        initializeComponent()
    }

    private fun initializeComponent(): AppComponent = DaggerAppComponent.factory().create(this)

    override fun providePaymentSubcomponent() =
        appComponent.providePaymentComponent().create()
}