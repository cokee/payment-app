package cl.jaa.projects.payment.presentation.selectinstallment.reducer

import cl.jaa.projects.core.mvi.UnsupportedReduceException
import cl.jaa.projects.payment.presentation.selectinstallment.SelectInstallmentResult
import cl.jaa.projects.payment.presentation.selectinstallment.SelectInstallmentResult.GetInstallmentsResult
import cl.jaa.projects.payment.presentation.selectinstallment.SelectInstallmentUiState
import cl.jaa.projects.payment.presentation.selectinstallment.SelectInstallmentUiState.LoadingUiState
import cl.jaa.projects.payment.presentation.selectinstallment.SelectInstallmentUiStateReducer
import javax.inject.Inject

class ErrorUiStateReducer @Inject constructor() : SelectInstallmentUiStateReducer {
    override fun reduce(
        currentState: SelectInstallmentUiState,
        result: SelectInstallmentResult
    ): SelectInstallmentUiState {
        return when (result) {
            is GetInstallmentsResult.InProgress -> LoadingUiState
            else -> throw UnsupportedReduceException(currentState, result)
        }

    }

}