package cl.jaa.projects.core.mvi

import kotlinx.coroutines.flow.Flow

interface MviActionProcessor<in A : MviAction, R : MviResult> {
    suspend fun process(action: A): Flow<R>
}