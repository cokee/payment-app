package cl.jaa.projects.payment.ui.di

import androidx.lifecycle.ViewModelProvider
import cl.jaa.projects.payment.ui.entrypoint.EntryPointIntentHandler
import cl.jaa.projects.payment.ui.selectbank.SelectBankIntentHandler
import cl.jaa.projects.payment.ui.selectinstallment.SelectInstallmentIntentHandler
import cl.jaa.projects.payment.ui.selectpayment.SelectPaymentIntentHandler
import dagger.Subcomponent
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview

@FlowPreview
@Subcomponent(
    modules = [
        CacheModule::class,
        RemoteModule::class,
        PresentationModule::class
    ]
)
interface PaymentSubcomponent {

    @Subcomponent.Factory
    interface Factory {
        fun create(): PaymentSubcomponent
    }

    fun getViewModelFactory(): ViewModelProvider.Factory
    fun getEntryPointIntentHandler(): EntryPointIntentHandler
    fun getSelectPaymentIntentHandler(): SelectPaymentIntentHandler
    fun getSelectBankIntentHandler(): SelectBankIntentHandler
    fun getSelectInstallmentIntentHandler(): SelectInstallmentIntentHandler
}
