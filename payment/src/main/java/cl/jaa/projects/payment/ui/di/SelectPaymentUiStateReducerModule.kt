package cl.jaa.projects.payment.ui.di

import cl.jaa.projects.payment.presentation.selectpayment.SelectPaymentUiReducer
import cl.jaa.projects.payment.presentation.selectpayment.SelectPaymentUiState
import cl.jaa.projects.payment.presentation.selectpayment.reducer.DefaultUiStateReducer
import cl.jaa.projects.payment.presentation.selectpayment.reducer.DisplayPaymentMethodsUiStateReducer
import cl.jaa.projects.payment.presentation.selectpayment.reducer.ErrorUiStateReducer
import cl.jaa.projects.payment.presentation.selectpayment.reducer.LoadingUiStateReducer
import dagger.Binds
import dagger.MapKey
import dagger.Module
import dagger.multibindings.IntoMap
import kotlin.reflect.KClass

@Module
abstract class SelectPaymentUiStateReducerModule {
    @Binds
    @IntoMap
    @SelectPaymentUiStateReducerKey(SelectPaymentUiState.DefaultUiState::class)
    abstract fun bindDefaultUiStateReducer(reducer: DefaultUiStateReducer): SelectPaymentUiReducer

    @Binds
    @IntoMap
    @SelectPaymentUiStateReducerKey(SelectPaymentUiState.LoadingUiState::class)
    abstract fun bindLoadingUiStateReducer(reducer: LoadingUiStateReducer): SelectPaymentUiReducer

    @Binds
    @IntoMap
    @SelectPaymentUiStateReducerKey(SelectPaymentUiState.DisplayPaymentMethodsUiState::class)
    abstract fun bindDisplayPaymentMethodsUiStateReducer(reducer: DisplayPaymentMethodsUiStateReducer): SelectPaymentUiReducer

    @Binds
    @IntoMap
    @SelectPaymentUiStateReducerKey(SelectPaymentUiState.ErrorUiState::class)
    abstract fun bindErrorUiStateReducer(reducer: ErrorUiStateReducer): SelectPaymentUiReducer

}

@Retention(AnnotationRetention.RUNTIME)
@Target(
    AnnotationTarget.FUNCTION,
    AnnotationTarget.PROPERTY_GETTER,
    AnnotationTarget.PROPERTY_SETTER
)
@MapKey
annotation class SelectPaymentUiStateReducerKey(val kclass: KClass<out SelectPaymentUiState>)