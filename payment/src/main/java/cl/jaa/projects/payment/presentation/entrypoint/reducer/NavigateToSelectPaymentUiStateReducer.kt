package cl.jaa.projects.payment.presentation.entrypoint.reducer

import cl.jaa.projects.core.mvi.UnsupportedReduceException
import cl.jaa.projects.payment.presentation.entrypoint.EntryPointResult
import cl.jaa.projects.payment.presentation.entrypoint.EntryPointUiState
import cl.jaa.projects.payment.presentation.entrypoint.EntryPointUiStateReducer
import javax.inject.Inject

class NavigateToSelectPaymentUiStateReducer @Inject constructor(): EntryPointUiStateReducer {
    override fun reduce(
        currentState: EntryPointUiState,
        result: EntryPointResult
    ): EntryPointUiState {
        throw UnsupportedReduceException(currentState, result)
    }
}