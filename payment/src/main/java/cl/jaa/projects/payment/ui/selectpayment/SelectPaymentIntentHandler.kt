package cl.jaa.projects.payment.ui.selectpayment

import cl.jaa.projects.payment.presentation.selectpayment.SelectPaymentUIntent
import cl.jaa.projects.payment.ui.commons.CardItem
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

class SelectPaymentIntentHandler @Inject constructor() {

    private val userIntents = MutableSharedFlow<SelectPaymentUIntent>()
    var coroutineScope: CoroutineScope? = null

    fun userIntents(): Flow<SelectPaymentUIntent> = merge(
        flow { emit(SelectPaymentUIntent.SeePaymentMethodsUIntent) },
        userIntents.asSharedFlow()
    )

    fun saveCardSelected(id: CardItem) {
        coroutineScope?.launch {
            userIntents.emit(SelectPaymentUIntent.SelectedPaymentMethodUIntent(id))
        }
    }
}
