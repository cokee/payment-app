package cl.jaa.projects.core.mvi

interface MviReducer<TUiState : MviUiState, TResult : MviResult> {

    fun reduce(currentState: TUiState, result: TResult): TUiState
}