package cl.jaa.projects.payment.presentation.selectpayment.reducer

import cl.jaa.projects.core.mvi.UnsupportedReduceException
import cl.jaa.projects.payment.presentation.selectpayment.SelectPaymentResult
import cl.jaa.projects.payment.presentation.selectpayment.SelectPaymentResult.GetPaymentMethodsResult
import cl.jaa.projects.payment.presentation.selectpayment.SelectPaymentUiReducer
import cl.jaa.projects.payment.presentation.selectpayment.SelectPaymentUiState
import javax.inject.Inject

class DefaultUiStateReducer @Inject constructor() : SelectPaymentUiReducer {
    override fun reduce(
        currentState: SelectPaymentUiState,
        result: SelectPaymentResult
    ): SelectPaymentUiState {
        return when (result) {
            is GetPaymentMethodsResult.InProgress -> SelectPaymentUiState.LoadingUiState
            else -> throw UnsupportedReduceException(currentState, result)
        }
    }
}