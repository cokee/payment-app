package cl.jaa.projects.payment.presentation.selectbank

import cl.jaa.projects.core.mvi.MviProcessor
import javax.inject.Inject
import javax.inject.Provider

class SelectBankProcessor @Inject constructor(
    processors: Map<Class<out SelectBankAction>, @JvmSuppressWildcards Provider<SelectBankActionProcessor<SelectBankAction, SelectBankResult>>>
) : MviProcessor<SelectBankAction, SelectBankResult, SelectBankActionProcessor<SelectBankAction, SelectBankResult>>(
    processors = processors,
    defaultResult = SelectBankResult.NoneResult
)
