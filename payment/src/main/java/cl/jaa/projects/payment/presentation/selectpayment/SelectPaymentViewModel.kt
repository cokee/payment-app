package cl.jaa.projects.payment.presentation.selectpayment

import cl.jaa.projects.payment.presentation.selectpayment.SelectPaymentAction.GetPaymentMethodsAction
import cl.jaa.projects.payment.presentation.selectpayment.SelectPaymentAction.SaveSelectedPaymentMethodAction
import cl.jaa.projects.payment.presentation.selectpayment.SelectPaymentResult.SaveSelectedPaymentMethodResult.*
import cl.jaa.projects.payment.presentation.selectpayment.SelectPaymentUIntent.*
import cl.jaa.projects.payment.ui.commons.MviViewModel
import cl.jaa.projects.payment.ui.navigation.NavActions
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@FlowPreview
class SelectPaymentViewModel @Inject constructor(
    reducer: SelectPaymentReducer,
    processor: SelectPaymentProcessor
) : MviViewModel<SelectPaymentUiReducer,
        SelectPaymentUiState,
        SelectPaymentResult,
        SelectPaymentAction,
        SelectPaymentProcessor,
        SelectPaymentUIntent>(
    reducer,
    processor,
    SelectPaymentUiState.DefaultUiState
) {
    var navActions: NavActions? = null

    override fun SelectPaymentUIntent.toAction(): SelectPaymentAction {
        return when (this) {
            SeePaymentMethodsUIntent, RetrySeeUIntent -> GetPaymentMethodsAction
            is SelectedPaymentMethodUIntent -> SaveSelectedPaymentMethodAction(
                selectedPaymentId
            )
        }
    }

    override fun Flow<SelectPaymentResult>.checkResultToNavigation(): Flow<SelectPaymentResult> =
        onEach { result ->
            when (result) {
                Success -> {
                    navActions?.bank?.invoke()
                }
                else -> return@onEach
            }
        }
}
