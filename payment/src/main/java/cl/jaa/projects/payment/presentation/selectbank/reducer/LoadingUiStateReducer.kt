package cl.jaa.projects.payment.presentation.selectbank.reducer

import cl.jaa.projects.core.mvi.UnsupportedReduceException
import cl.jaa.projects.payment.presentation.selectbank.SelectBankResult
import cl.jaa.projects.payment.presentation.selectbank.SelectBankResult.GetBanksResult.Failure
import cl.jaa.projects.payment.presentation.selectbank.SelectBankResult.GetBanksResult.Success
import cl.jaa.projects.payment.presentation.selectbank.SelectBankUiState
import cl.jaa.projects.payment.presentation.selectbank.SelectBankUiState.DisplayBanksUiState
import cl.jaa.projects.payment.presentation.selectbank.SelectBankUiState.ErrorUiState
import cl.jaa.projects.payment.presentation.selectbank.SelectBankUiStateReducer
import javax.inject.Inject

class LoadingUiStateReducer @Inject constructor() : SelectBankUiStateReducer {
    override fun reduce(
        currentState: SelectBankUiState,
        result: SelectBankResult
    ): SelectBankUiState {
        return when (result) {
            is Success -> DisplayBanksUiState(result.items)
            is Failure -> ErrorUiState
            else -> throw UnsupportedReduceException(currentState, result)
        }
    }
}