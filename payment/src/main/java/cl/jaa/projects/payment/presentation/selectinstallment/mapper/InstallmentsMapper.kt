package cl.jaa.projects.payment.presentation.selectinstallment.mapper

import cl.jaa.projects.payment.data.remote.model.RemotePayerCost
import cl.jaa.projects.payment.ui.commons.SpinnerItem
import javax.inject.Inject

class InstallmentsMapper @Inject constructor() {

    fun List<RemotePayerCost>.toPresentation() = map { remotePayerCost ->
        remotePayerCost.toPresentation()
    }

    private fun RemotePayerCost.toPresentation() = SpinnerItem(
        id = installments.toString(),
        title = recommendedMessage.toString()
    )
}