package cl.jaa.projects.payment.data.remote.retrofit

import cl.jaa.projects.payment.data.remote.model.RemoteCardIssuersItem
import cl.jaa.projects.payment.data.remote.model.RemoteInstallmentsItem
import cl.jaa.projects.payment.data.remote.model.RemotePaymentItem
import retrofit2.http.GET
import retrofit2.http.Query

interface PaymentWebService {

    @GET("v1/payment_methods")
    suspend fun getPaymentMethods(@Query("public_key") publicKey: String): List<RemotePaymentItem>

    @GET("v1/payment_methods/card_issuers")
    suspend fun getCardIssuers(
        @Query("public_key") publicKey: String,
        @Query("payment_method_id") paymentMethodId: String
    ): List<RemoteCardIssuersItem>

    @GET("v1/payment_methods/installments")
    suspend fun getInstallments(
        @Query("public_key") publicKey: String,
        @Query("payment_method_id") paymentMethodId: String,
        @Query("amount") amount: String,
        @Query("issuer.id") issuerId: String
    ): List<RemoteInstallmentsItem>
}