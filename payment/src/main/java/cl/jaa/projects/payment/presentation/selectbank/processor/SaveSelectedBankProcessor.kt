package cl.jaa.projects.payment.presentation.selectbank.processor

import cl.jaa.projects.payment.data.PaymentRepository
import cl.jaa.projects.payment.presentation.selectbank.SelectBankAction
import cl.jaa.projects.payment.presentation.selectbank.SelectBankActionProcessor
import cl.jaa.projects.payment.presentation.selectbank.SelectBankResult
import cl.jaa.projects.payment.presentation.selectbank.mapper.CardIssuersMapper
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class SaveSelectedBankProcessor @Inject constructor(
    private val repository: PaymentRepository,
    private val cardIssuersMapper: CardIssuersMapper
) : SelectBankActionProcessor<SelectBankAction, SelectBankResult> {
    override suspend fun process(action: SelectBankAction): Flow<SelectBankResult> = flow {
        val actionResult = action as SelectBankAction.SaveSelectedGetBanksAction
        repository.saveBank(with(cardIssuersMapper) { actionResult.item.toRemote() })
        emit(SelectBankResult.SaveBankSelectedResult.Success)
    }
}