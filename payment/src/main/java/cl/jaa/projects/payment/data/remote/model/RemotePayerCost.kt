package cl.jaa.projects.payment.data.remote.model


import cl.jaa.projects.payment.data.remote.model.Constants.INSTALLMENTS
import cl.jaa.projects.payment.data.remote.model.Constants.RECOMMENDED_MESSAGE
import com.google.gson.annotations.SerializedName

data class RemotePayerCost(
    @SerializedName(INSTALLMENTS) val installments: Int?,
    @SerializedName(RECOMMENDED_MESSAGE) val recommendedMessage: String?,
)