package cl.jaa.projects.payment.ui.di

import cl.jaa.projects.payment.presentation.selectinstallment.SelectInstallmentAction
import cl.jaa.projects.payment.presentation.selectinstallment.SelectInstallmentActionProcessor
import cl.jaa.projects.payment.presentation.selectinstallment.SelectInstallmentResult
import cl.jaa.projects.payment.presentation.selectinstallment.processor.GetInstallmentsProcessor
import cl.jaa.projects.payment.presentation.selectinstallment.processor.SaveSelectedInstallmentProcessor
import dagger.Binds
import dagger.MapKey
import dagger.Module
import dagger.multibindings.IntoMap
import kotlin.reflect.KClass

@Module
abstract class SelectInstallmentActionProcessorModule {

    @Binds
    @IntoMap
    @SelectInstallmentActionProcessorKey(SelectInstallmentAction.GetInstallmentsAction::class)
    abstract fun bindGetBankProcessor(processor: GetInstallmentsProcessor):
            SelectInstallmentActionProcessor<SelectInstallmentAction, SelectInstallmentResult>

    @Binds
    @IntoMap
    @SelectInstallmentActionProcessorKey(SelectInstallmentAction.SaveSelectedInstallmentAction::class)
    abstract fun bindSaveSelectedInstallmentProcessor(processor: SaveSelectedInstallmentProcessor):
            SelectInstallmentActionProcessor<SelectInstallmentAction, SelectInstallmentResult>

}

@MustBeDocumented
@Retention(AnnotationRetention.RUNTIME)
@Target(
    AnnotationTarget.FUNCTION,
    AnnotationTarget.PROPERTY_GETTER,
    AnnotationTarget.PROPERTY_SETTER
)
@MapKey
annotation class SelectInstallmentActionProcessorKey(val kclass: KClass<out SelectInstallmentAction>)