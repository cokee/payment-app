package cl.jaa.projects.payment.presentation.selectpayment.mapper

import cl.jaa.projects.payment.data.remote.model.RemotePaymentItem
import cl.jaa.projects.payment.presentation.selectpayment.model.PaymentMethodsItem
import cl.jaa.projects.payment.ui.commons.CardItem
import javax.inject.Inject

class PaymentMethodsMapper @Inject constructor() {

    fun List<RemotePaymentItem>.toPresentation(): List<CardItem> = map { remote ->
        CardItem(
            id = remote.id.orEmpty(),
            title = remote.name.orEmpty(),
            thumbnail = remote.thumbnail.orEmpty()
        )
    }
    fun CardItem.toRemote() = RemotePaymentItem(
        id = id,
        name = title,
        thumbnail = thumbnail
    )
}
