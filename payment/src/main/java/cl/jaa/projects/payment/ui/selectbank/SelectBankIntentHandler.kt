package cl.jaa.projects.payment.ui.selectbank

import cl.jaa.projects.payment.presentation.selectbank.SelectBankUIntent
import cl.jaa.projects.payment.ui.commons.CardItem
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

class SelectBankIntentHandler @Inject constructor() {

    private val userIntents = MutableSharedFlow<SelectBankUIntent>()
    var coroutineScope: CoroutineScope? = null

    fun userIntents(): Flow<SelectBankUIntent> = merge(
        flow { emit(SelectBankUIntent.SeeBanksUIntent) },
        userIntents.asSharedFlow()
    )

    fun saveBankSelected(selectedBankItem: CardItem) {
        coroutineScope?.launch {
            userIntents.emit(SelectBankUIntent.SelectedBankUIntent(selectedBankItem))
        }
    }
}
