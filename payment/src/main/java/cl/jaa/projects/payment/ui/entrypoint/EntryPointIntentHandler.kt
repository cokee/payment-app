package cl.jaa.projects.payment.ui.entrypoint

import cl.jaa.projects.payment.presentation.entrypoint.EntryPointUIntent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.merge
import kotlinx.coroutines.launch
import javax.inject.Inject

class EntryPointIntentHandler @Inject constructor() {

    private val userIntents = MutableSharedFlow<EntryPointUIntent>()
    var coroutineScope: CoroutineScope? = null

    fun userIntents(): Flow<EntryPointUIntent> = merge(
        userIntents.asSharedFlow()
    )

    fun saveAmountUIntent(amount: String) {
        coroutineScope?.launch {
            userIntents.emit(EntryPointUIntent.ContinueNextStepUIntent(amount))
        }
    }
}