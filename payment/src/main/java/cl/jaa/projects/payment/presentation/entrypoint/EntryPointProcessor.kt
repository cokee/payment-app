package cl.jaa.projects.payment.presentation.entrypoint

import cl.jaa.projects.core.mvi.MviProcessor
import javax.inject.Inject
import javax.inject.Provider

class EntryPointProcessor @Inject constructor(
    processors: Map<Class<out EntryPointAction>, @JvmSuppressWildcards Provider<EntryPointActionProcessor<EntryPointAction, EntryPointResult>>>
) : MviProcessor<EntryPointAction, EntryPointResult, EntryPointActionProcessor<EntryPointAction, EntryPointResult>>(
    processors = processors,
    defaultResult = EntryPointResult.NoneResult
)
