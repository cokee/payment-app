package cl.jaa.projects.payment.data.cache

import cl.jaa.projects.payment.data.cache.datastore.PaymentDataStore
import cl.jaa.projects.payment.data.source.PaymentCache
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.take
import javax.inject.Inject

class PaymentCacheImpl @Inject constructor(
    private val paymentDataStore: PaymentDataStore
) : PaymentCache {
    override suspend fun saveAmount(amount: String) {
        paymentDataStore.saveAmount(amount)
    }

    override suspend fun saveCard(card: String) {
        paymentDataStore.saveCard(card)
    }

    override suspend fun saveBank(bank: String) {
        paymentDataStore.saveBank(bank)
    }

    override suspend fun saveInstallments(installments: String) {
        paymentDataStore.saveInstallments(installments)
    }

    override suspend fun getAmount(): Flow<String> =
        paymentDataStore.getAmount().take(1)

    override suspend fun getCard(): Flow<String> =
        paymentDataStore.getCard().take(1)

    override suspend fun getBank(): Flow<String> =
        paymentDataStore.getBank().take(1)

    override suspend fun getInstallments(): Flow<String> =
        paymentDataStore.getInstallments().take(1)

}
