package cl.jaa.projects.payment.presentation.selectpayment.model

data class PaymentMethodsItem(
    val id: String,
    val maxAllowedAmount: Int,
    val name: String,
    val thumbnail: String
)

