package cl.jaa.projects.payment.presentation.entrypoint

import cl.jaa.projects.core.mvi.MviResult

sealed class EntryPointResult : MviResult {
    object NoneResult : SaveAmountResult()
    sealed class SaveAmountResult : EntryPointResult() {
        object Success : SaveAmountResult()
    }
}
