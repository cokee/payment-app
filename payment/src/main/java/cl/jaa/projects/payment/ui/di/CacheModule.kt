package cl.jaa.projects.payment.ui.di

import cl.jaa.projects.payment.data.cache.PaymentCacheImpl
import cl.jaa.projects.payment.data.source.PaymentCache
import dagger.Binds
import dagger.Module

@Module
abstract class CacheModule {

    @Binds
    abstract fun bindCache(cacheImpl: PaymentCacheImpl): PaymentCache
}