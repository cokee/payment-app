package cl.jaa.projects.payment.presentation.selectinstallment.reducer

import cl.jaa.projects.core.mvi.UnsupportedReduceException
import cl.jaa.projects.payment.presentation.selectinstallment.SelectInstallmentResult
import cl.jaa.projects.payment.presentation.selectinstallment.SelectInstallmentUiState
import cl.jaa.projects.payment.presentation.selectinstallment.SelectInstallmentUiStateReducer
import javax.inject.Inject

class DisplayPaymentParamsUiStateReducer @Inject constructor() : SelectInstallmentUiStateReducer {
    override fun reduce(
        currentState: SelectInstallmentUiState,
        result: SelectInstallmentResult
    ): SelectInstallmentUiState {
        return when (result) {
            is SelectInstallmentResult.SaveInstallmentSelectedResult.Success -> currentState
            else -> throw UnsupportedReduceException(currentState, result)
        }
    }
}