package cl.jaa.projects.payment.presentation.selectinstallment

import cl.jaa.projects.core.mvi.MviActionProcessor

interface SelectInstallmentActionProcessor<A: SelectInstallmentAction, R: SelectInstallmentResult>: MviActionProcessor<A, R>