package cl.jaa.projects.payment.data.cache.datastore.config

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.preferencesDataStore
import javax.inject.Inject

class PaymentDataStoreFactory @Inject constructor(
    context: Context
) {

    companion object {
        const val NAME = "PaymentPreferences"
        const val AMOUNT = "amount"
        const val CARD = "card"
        const val BANK = "bank"
        const val INSTALLMENTS = "installments"

        val Context.dataStore: DataStore<Preferences> by preferencesDataStore(
            name = NAME
        )
    }

    val getDataStore = context.dataStore
}
