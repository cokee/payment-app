package cl.jaa.projects.payment.data.cache

import cl.jaa.projects.payment.data.cache.datastore.PaymentDataStore
import io.mockk.InternalPlatformDsl.toStr
import io.mockk.coEvery
import io.mockk.coJustRun
import io.mockk.coVerify
import io.mockk.mockk
import junit.framework.Assert.assertTrue
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.runBlocking
import org.junit.Test

class PaymentCacheImplTest {
    private val dataStore = mockk<PaymentDataStore>()

    private val cache = PaymentCacheImpl(
        paymentDataStore = dataStore
    )

    @Test
    fun `given amount, when saveAmount, then save on datastore`() = runBlocking {
        val amount = Math.random().toStr()
        stubSaveAmount(amount)

        cache.saveAmount(amount)

        coVerify { dataStore.saveAmount(amount) }
    }

    @Test
    fun `given card, when saveCard, then save on datastore`() = runBlocking {
        val card = "visa"
        stubSaveCard(card)

        cache.saveCard(card)

        coVerify { dataStore.saveCard(card) }
    }

    @Test
    fun `given card, when saveBank, then save on datastore`() = runBlocking {
        val bank = "santander"
        stubSaveBank(bank)

        cache.saveBank(bank)

        coVerify { dataStore.saveBank(bank) }
    }

    @Test
    fun `given card, when saveInstallments, then save on datastore`() = runBlocking {
        val installments = "2"
        stubSaveInstallments(installments)

        cache.saveInstallments(installments)

        coVerify { dataStore.saveInstallments(installments) }
    }

    @Test
    fun `given amount saved, when getAmount, then return amount saved`() = runBlocking {
        val amountSaved = Math.random().toStr()
        stubGetAmount(amountSaved)

        val amount = cache.getAmount().first()

        assertTrue(amount == amountSaved)
    }

    @Test
    fun `given card saved, when getCard, then return card saved`() = runBlocking {
        val cardSaved = "visa"
        stubGetCard(cardSaved)

        val card = cache.getCard().first()

        assertTrue(card == cardSaved)
    }

    @Test
    fun `given bank saved, when getBank, then return bank saved`() = runBlocking {
        val bankSaved = "satander"
        stubGetBank(bankSaved)

        val bank = cache.getBank().first()

        assertTrue(bank == bankSaved)
    }

    @Test
    fun `given installments saved, when getInstallments, then return installments saved`() =
        runBlocking {
            val installmentsSaved = "2"
            stubGetInstallments(installmentsSaved)

            val installments = cache.getInstallments().first()

            assertTrue(installments == installmentsSaved)
        }

    private fun stubSaveCard(card: String) {
        coJustRun { dataStore.saveCard(card) }
    }

    private fun stubSaveAmount(amount: String) {
        coJustRun { dataStore.saveAmount(amount) }
    }

    private fun stubSaveBank(bank: String) {
        coJustRun { dataStore.saveBank(bank) }
    }

    private fun stubSaveInstallments(installments: String) {
        coJustRun { dataStore.saveInstallments(installments) }
    }

    private fun stubGetAmount(amount: String) {
        coEvery { dataStore.getAmount() } returns flow { emit(amount) }
    }

    private fun stubGetCard(card: String) {
        coEvery { dataStore.getCard() } returns flow { emit(card) }
    }

    private fun stubGetBank(bank: String) {
        coEvery { dataStore.getBank() } returns flow { emit(bank) }
    }

    private fun stubGetInstallments(installments: String) {
        coEvery { dataStore.getInstallments() } returns flow { emit(installments) }
    }
}