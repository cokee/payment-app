package cl.jaa.projects.payment.presentation.selectinstallment

import cl.jaa.projects.core.mvi.MviResult
import cl.jaa.projects.payment.presentation.selectinstallment.model.PaymentParams
import cl.jaa.projects.payment.ui.commons.SpinnerItem

sealed class SelectInstallmentResult : MviResult {
    object NoneResult : SelectInstallmentResult()
    sealed class GetInstallmentsResult : SelectInstallmentResult() {
        data class Success(val items: List<SpinnerItem>) : GetInstallmentsResult()
        object InProgress : GetInstallmentsResult()
        object Failure : GetInstallmentsResult()
    }

    sealed class SaveInstallmentSelectedResult : SelectInstallmentResult() {
        data class Success(val paymentParams: PaymentParams) : SaveInstallmentSelectedResult()
    }
}
