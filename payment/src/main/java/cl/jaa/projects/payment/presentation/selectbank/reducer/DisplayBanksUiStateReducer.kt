package cl.jaa.projects.payment.presentation.selectbank.reducer

import cl.jaa.projects.core.mvi.UnsupportedReduceException
import cl.jaa.projects.payment.presentation.selectbank.SelectBankResult
import cl.jaa.projects.payment.presentation.selectbank.SelectBankUiState
import cl.jaa.projects.payment.presentation.selectbank.SelectBankUiStateReducer
import javax.inject.Inject

class DisplayBanksUiStateReducer @Inject constructor() : SelectBankUiStateReducer {
    override fun reduce(
        currentState: SelectBankUiState,
        result: SelectBankResult
    ): SelectBankUiState {
        return when (result) {
            SelectBankResult.SaveBankSelectedResult.Success -> currentState
            else -> throw UnsupportedReduceException(currentState, result)
        }
    }
}