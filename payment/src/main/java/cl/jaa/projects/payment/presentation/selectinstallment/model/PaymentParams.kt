package cl.jaa.projects.payment.presentation.selectinstallment.model

data class PaymentParams(
    val amount: String,
    val card: String,
    val bank: String,
    val installments: String
)

