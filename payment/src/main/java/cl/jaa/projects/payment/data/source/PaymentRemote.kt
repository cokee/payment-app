package cl.jaa.projects.payment.data.source

import cl.jaa.projects.payment.data.remote.model.RemoteCardIssuersItem
import cl.jaa.projects.payment.data.remote.model.RemoteInstallmentsItem
import cl.jaa.projects.payment.data.remote.model.RemotePayerCost
import cl.jaa.projects.payment.data.remote.model.RemotePaymentItem

interface PaymentRemote {
    suspend fun getRemotePaymentMethods(): List<RemotePaymentItem>
    suspend fun getCardIssuers(paymentMethodId: String): List<RemoteCardIssuersItem>
    suspend fun getInstallments(
        paymentMethodId: String,
        issuerId: String,
        amount: String
    ): List<RemotePayerCost>
}
