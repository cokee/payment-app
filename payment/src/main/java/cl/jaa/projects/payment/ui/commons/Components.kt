package cl.jaa.projects.payment.ui.commons

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowDropDown
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import cl.jaa.projects.payment.R
import cl.jaa.projects.payment.presentation.selectinstallment.model.PaymentParams
import coil.compose.AsyncImage
import com.google.accompanist.insets.navigationBarsPadding
import com.google.accompanist.insets.statusBarsPadding

@Composable
fun PaymentTopAppBar(title: String) {
    TopAppBar(
        title = {
            Text(text = title)
        },
        backgroundColor = Color(0xff009ee3),
        contentColor = contentColorFor(backgroundColor = Color(0xff009ee3)),
        elevation = dimensionResource(id = R.dimen.top_bar_elevation),
        modifier = Modifier
            .statusBarsPadding()
            .navigationBarsPadding(bottom = true)
    )
}

@Composable
fun DefaultContent() {
    Column {
    }
}

@Composable
fun LoadingComponent() {
    CircularProgressIndicator()
}

@Composable
fun ErrorComponent(errorText: String = "") {
    Text(text = errorText.ifEmpty { stringResource(id = R.string.error_text) })
}

@Composable
fun DisplaySelectableCardItems(
    headerLabel: String = "",
    cardItems: List<CardItem>,
    selectCardEvent: (cardItem: CardItem) -> Unit
) {
    val listState = rememberLazyListState()

    LazyColumn(
        state = listState,
        verticalArrangement = Arrangement.SpaceBetween
    ) {
        item {
            Text(
                text = headerLabel,
                fontWeight = FontWeight.Bold,
                fontSize = 30.sp,
                modifier = Modifier.padding(8.dp),
                textAlign = TextAlign.Center
            )
        }
        items(cardItems) { paymentMethod ->
            CardPaymentSelectorItem(
                cardItem = paymentMethod,
                selectCardEvent = selectCardEvent
            )
        }
    }
}

data class CardItem(
    val id: String,
    val title: String,
    val subTitle: String = "",
    val thumbnail: String
)

@Composable
private fun CardPaymentSelectorItem(
    cardItem: CardItem,
    selectCardEvent: (cardItem: CardItem) -> Unit
) {
    Card(
        modifier = Modifier
            .clickable { selectCardEvent(cardItem) }
            .padding(8.dp)
            .fillMaxWidth(),
        elevation = 8.dp
    ) {
        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier.padding(8.dp)
        ) {
            AsyncImage(
                model = cardItem.thumbnail,
                contentDescription = stringResource(id = R.string.left_image_card),
                modifier = Modifier
                    .width(100.dp)
            )
            Column(modifier = Modifier.padding(start = 8.dp)) {
                Text(
                    text = cardItem.title,
                    fontWeight = FontWeight.Bold,
                )
                Text(
                    text = cardItem.subTitle
                )
            }
        }
    }
}

@Composable
@Preview
private fun previewCardPaymentSelectorItem() {
    CardPaymentSelectorItem(
        CardItem(
            id = "1",
            subTitle = "maximo permitido $ 2000",
            title = "Visa",
            thumbnail = "http://img.mlstatic.com/org-img/MP3/API/logos/visa.gif"
        ),
        selectCardEvent = {}
    )
}

data class SpinnerItem(
    val id: String,
    val title: String
)

@Composable
fun SpinnerSelection(
    spinnerItems: List<SpinnerItem>,
    selectedItemEvent: (spinnerItem: SpinnerItem) -> Unit,
    headerLabel: String
) {
    var spinnerTitle: String by remember { mutableStateOf("Selecciona una cuota") }
    var expanded by remember { mutableStateOf(false) }

    Column {
        Text(
            text = headerLabel,
            fontWeight = FontWeight.Bold,
            fontSize = 30.sp,
            modifier = Modifier.padding(8.dp),
            textAlign = TextAlign.Center
        )
        Box(Modifier.fillMaxWidth(), contentAlignment = Alignment.Center) {
            Row(
                Modifier
                    .padding(24.dp)
                    .clickable {
                        expanded = !expanded
                    }
                    .padding(8.dp),
                horizontalArrangement = Arrangement.Center,
                verticalAlignment = Alignment.CenterVertically
            ) {
                Text(
                    text = spinnerTitle,
                    fontSize = 18.sp,
                    modifier = Modifier.padding(end = 8.dp)
                )
                Icon(imageVector = Icons.Filled.ArrowDropDown, contentDescription = "")
                DropdownMenu(expanded = expanded, onDismissRequest = {
                    expanded = false
                }) {
                    spinnerItems.forEach { item ->
                        DropdownMenuItem(onClick = {
                            expanded = false
                            spinnerTitle = item.title
                            selectedItemEvent(item)
                        }) {
                            Text(text = item.title)
                        }
                    }
                }
            }
        }
    }
}

@Composable
fun DisplayReceipt(params: PaymentParams, receiptEvent: () -> Unit) {
    Column(
        verticalArrangement = Arrangement.SpaceBetween,
        modifier = Modifier.padding(8.dp)
    ) {
        Text(
            text = stringResource(id = R.string.amount_label).format(params.amount),
            fontSize = 15.sp
        )
        Text(
            text = stringResource(id = R.string.card_label).format(params.card),
            fontSize = 15.sp
        )
        Text(
            text = stringResource(id = R.string.bank_label).format(params.bank),
            fontSize = 15.sp
        )
        Text(
            text = stringResource(id = R.string.installment_label).format(params.installments),
            fontSize = 15.sp
        )

        Button(onClick = receiptEvent, modifier = Modifier.fillMaxWidth().padding(8.dp)) {
            Text(text = stringResource(id = R.string.back_to_init))
        }
    }
}