package cl.jaa.projects.payment.presentation.selectbank.reducer

import cl.jaa.projects.core.mvi.UnsupportedReduceException
import cl.jaa.projects.payment.presentation.selectbank.SelectBankResult
import cl.jaa.projects.payment.presentation.selectbank.SelectBankResult.GetBanksResult.InProgress
import cl.jaa.projects.payment.presentation.selectbank.SelectBankUiState
import cl.jaa.projects.payment.presentation.selectbank.SelectBankUiState.LoadingUiState
import cl.jaa.projects.payment.presentation.selectbank.SelectBankUiStateReducer
import javax.inject.Inject

class DefaultUiStateReducer @Inject constructor() : SelectBankUiStateReducer {
    override fun reduce(
        currentState: SelectBankUiState,
        result: SelectBankResult
    ): SelectBankUiState {
        return when(result){
            is InProgress -> LoadingUiState
            else -> throw UnsupportedReduceException(currentState, result)
        }
    }
}