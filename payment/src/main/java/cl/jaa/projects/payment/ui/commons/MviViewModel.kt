package cl.jaa.projects.payment.ui.commons

import androidx.lifecycle.ViewModel
import cl.jaa.projects.core.mvi.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.buffer
import kotlinx.coroutines.flow.flatMapMerge
import kotlinx.coroutines.flow.scan

abstract class MviViewModel<TReducer : MviReducer<TUiState,
        TResult>, TUiState : MviUiState,
        TResult : MviResult,
        TAction : MviAction,
        TActionProcessor : MviActionProcessor<TAction, TResult>,
        TUserIntent : MviUserIntent>(
    private val reducer: TReducer,
    private val processor: TActionProcessor,
    val defaultUiState: TUiState
) : ViewModel(), MviPresentation<TUserIntent, TUiState> {
    override fun processUserIntentsAndObserveUiStates(userIntents: Flow<TUserIntent>): Flow<TUiState> =
        userIntents.buffer()
            .flatMapMerge { userIntent ->
                processor.process(userIntent.toAction())
            }.checkResultToNavigation()
            .scan(defaultUiState) { previousUiState, result ->
                reducer.reduce(previousUiState, result)
            }

    abstract fun TUserIntent.toAction(): TAction

    abstract fun Flow<TResult>.checkResultToNavigation(): Flow<TResult>
}