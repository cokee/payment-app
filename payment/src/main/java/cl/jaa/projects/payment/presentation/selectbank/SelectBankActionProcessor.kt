package cl.jaa.projects.payment.presentation.selectbank

import cl.jaa.projects.core.mvi.MviActionProcessor

interface SelectBankActionProcessor<A: SelectBankAction, R: SelectBankResult>: MviActionProcessor<A, R>