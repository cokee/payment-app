package cl.jaa.projects.core.mvi

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf
import javax.inject.Provider

abstract class MviProcessor<TAction : MviAction, TResult : MviResult, TActionProcessor : MviActionProcessor<TAction, TResult>>(
    private val processors: Map<Class<out TAction>, @JvmSuppressWildcards Provider<TActionProcessor>>,
    private val defaultResult: TResult
) :
    MviActionProcessor<TAction, TResult> {
    override suspend fun process(action: TAction): Flow<TResult> {
        return processors[action::class.java]?.get()?.process(action) ?: flowOf(defaultResult)
    }

}