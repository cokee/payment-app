package cl.jaa.projects.payment.ui.navigation

import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.remember
import androidx.navigation.NavGraphBuilder
import androidx.navigation.compose.composable
import cl.jaa.projects.payment.presentation.entrypoint.EntryPointViewModel
import cl.jaa.projects.payment.presentation.selectbank.SelectBankViewModel
import cl.jaa.projects.payment.presentation.selectinstallment.SelectInstallmentViewModel
import cl.jaa.projects.payment.presentation.selectpayment.SelectPaymentViewModel
import cl.jaa.projects.payment.ui.entrypoint.EntryPointIntentHandler
import cl.jaa.projects.payment.ui.entrypoint.EntryPointScreen
import cl.jaa.projects.payment.ui.selectbank.BankScreen
import cl.jaa.projects.payment.ui.selectbank.SelectBankIntentHandler
import cl.jaa.projects.payment.ui.selectinstallment.InstallmentScreen
import cl.jaa.projects.payment.ui.selectinstallment.SelectInstallmentIntentHandler
import cl.jaa.projects.payment.ui.selectpayment.PaymentScreen
import cl.jaa.projects.payment.ui.selectpayment.SelectPaymentIntentHandler
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview

@ExperimentalCoroutinesApi
@FlowPreview
@ExperimentalAnimationApi
fun NavGraphBuilder.entryPointNav(
    viewModel: EntryPointViewModel,
    intentHandler: EntryPointIntentHandler
) =
    composable(
        route = Routes.EntryPoint.path
    ) {
        val entryPointUiState = remember {
            viewModel.processUserIntentsAndObserveUiStates(
                userIntents = intentHandler.userIntents()
            )
        }.collectAsState(initial = viewModel.defaultUiState)

        EntryPointScreen(
            saveAmountEvent = { amount ->
                intentHandler.saveAmountUIntent(amount)
            },
            uiState = entryPointUiState
        )
    }

@ExperimentalCoroutinesApi
@FlowPreview
@ExperimentalAnimationApi
fun NavGraphBuilder.paymentNav(
    viewModel: SelectPaymentViewModel,
    intentHandler: SelectPaymentIntentHandler
) =
    composable(
        route = Routes.Payment.path
    ) {
        val paymentUiState = remember {
            viewModel.processUserIntentsAndObserveUiStates(
                userIntents = intentHandler.userIntents()
            )
        }.collectAsState(initial = viewModel.defaultUiState)

        PaymentScreen(
            state = paymentUiState,
            selectCardEvent = { id ->
                intentHandler.saveCardSelected(id)
            }
        )
    }

@ExperimentalCoroutinesApi
@FlowPreview
@ExperimentalAnimationApi
fun NavGraphBuilder.bankNav(
    viewModel: SelectBankViewModel,
    intentHandler: SelectBankIntentHandler
) =
    composable(
        route = Routes.Bank.path
    ) {
        val bankUiState = remember {
            viewModel.processUserIntentsAndObserveUiStates(
                userIntents = intentHandler.userIntents()
            )
        }.collectAsState(initial = viewModel.defaultUiState)

        BankScreen(
            uiState = bankUiState,
            selectCardEvent = { id ->
                intentHandler.saveBankSelected(id)
            }
        )
    }

@ExperimentalCoroutinesApi
@FlowPreview
@ExperimentalAnimationApi
fun NavGraphBuilder.installmentNav(
    viewModel: SelectInstallmentViewModel,
    intentHandler: SelectInstallmentIntentHandler,
    actions: NavActions
) =
    composable(
        route = Routes.Installments.path
    ) {
        val installmentUiState = remember {
            viewModel.processUserIntentsAndObserveUiStates(
                userIntents = intentHandler.userIntents()
            )
        }.collectAsState(initial = viewModel.defaultUiState)

        InstallmentScreen(
            uiState = installmentUiState,
            selectCardEvent = { item ->
                intentHandler.saveInstallmentSelected(item)
            },
            receiptEvent = {
                actions.entryPoint.invoke()
            }
        )
    }
