package cl.jaa.projects.payment.presentation.selectbank

import cl.jaa.projects.core.mvi.MviResult
import cl.jaa.projects.payment.ui.commons.CardItem

sealed class SelectBankResult : MviResult {
    object NoneResult : SelectBankResult()
    sealed class GetBanksResult : SelectBankResult() {
        data class Success(val items: List<CardItem>) : GetBanksResult()
        object InProgress : GetBanksResult()
        object Failure : GetBanksResult()
    }

    sealed class SaveBankSelectedResult : SelectBankResult() {
        object Success : SaveBankSelectedResult()
    }
}
