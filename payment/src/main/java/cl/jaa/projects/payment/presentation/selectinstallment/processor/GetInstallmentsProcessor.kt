package cl.jaa.projects.payment.presentation.selectinstallment.processor

import android.util.Log
import cl.jaa.projects.payment.data.PaymentRepository
import cl.jaa.projects.payment.presentation.selectinstallment.SelectInstallmentAction
import cl.jaa.projects.payment.presentation.selectinstallment.SelectInstallmentActionProcessor
import cl.jaa.projects.payment.presentation.selectinstallment.SelectInstallmentResult
import cl.jaa.projects.payment.presentation.selectinstallment.mapper.InstallmentsMapper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import javax.inject.Inject

class GetInstallmentsProcessor @Inject constructor(
    private val repository: PaymentRepository,
    private val mapper: InstallmentsMapper
) : SelectInstallmentActionProcessor<SelectInstallmentAction, SelectInstallmentResult> {
    override suspend fun process(action: SelectInstallmentAction): Flow<SelectInstallmentResult> {
        return repository.getInstallments().map { payerCosts ->
            SelectInstallmentResult.GetInstallmentsResult.Success(
                with(mapper) { payerCosts.toPresentation() }
            ) as SelectInstallmentResult.GetInstallmentsResult
        }.onStart {
            emit(SelectInstallmentResult.GetInstallmentsResult.InProgress)
        }.catch { error ->
            Log.d(GetInstallmentsProcessor::class.simpleName, error.stackTraceToString())
            emit(SelectInstallmentResult.GetInstallmentsResult.Failure)
        }.flowOn(Dispatchers.IO)
    }
}