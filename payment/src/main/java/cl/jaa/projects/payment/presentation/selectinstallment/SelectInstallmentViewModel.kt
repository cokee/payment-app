package cl.jaa.projects.payment.presentation.selectinstallment

import cl.jaa.projects.payment.presentation.selectinstallment.SelectInstallmentUIntent.*
import cl.jaa.projects.payment.ui.commons.MviViewModel
import cl.jaa.projects.payment.ui.navigation.NavActions
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

@FlowPreview
class SelectInstallmentViewModel @Inject constructor(
    reducer: SelectInstallmentReducer,
    processor: SelectInstallmentProcessor
) : MviViewModel<SelectInstallmentUiStateReducer,
        SelectInstallmentUiState,
        SelectInstallmentResult,
        SelectInstallmentAction,
        SelectInstallmentProcessor,
        SelectInstallmentUIntent>(
    reducer,
    processor,
    SelectInstallmentUiState.DefaultUiState
) {
    var navActions: NavActions? = null

    override fun SelectInstallmentUIntent.toAction(): SelectInstallmentAction {
        return when (this) {
            is SeeInstallmentUIntent, RetrySeeInstallmentUIntent -> SelectInstallmentAction.GetInstallmentsAction
            is SelectedInstallmentUIntent -> SelectInstallmentAction.SaveSelectedInstallmentAction(
                selectedInstallment
            )
        }
    }

    override fun Flow<SelectInstallmentResult>.checkResultToNavigation(): Flow<SelectInstallmentResult> =
        this
}

