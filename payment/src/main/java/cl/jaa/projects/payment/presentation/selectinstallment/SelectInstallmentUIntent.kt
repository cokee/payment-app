package cl.jaa.projects.payment.presentation.selectinstallment

import cl.jaa.projects.core.mvi.MviUserIntent
import cl.jaa.projects.payment.ui.commons.SpinnerItem

sealed class SelectInstallmentUIntent : MviUserIntent {
    object SeeInstallmentUIntent : SelectInstallmentUIntent()
    data class SelectedInstallmentUIntent(val selectedInstallment: SpinnerItem) : SelectInstallmentUIntent()
    object RetrySeeInstallmentUIntent : SelectInstallmentUIntent()
}