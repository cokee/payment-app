package cl.jaa.projects.payment.data

import cl.jaa.projects.payment.data.cache.CachePaymentParams
import cl.jaa.projects.payment.factory.CachePaymentFactory.generateCachePaymentParams
import cl.jaa.projects.payment.factory.RemotePaymentFactory.generateCardIssuersItem
import cl.jaa.projects.payment.factory.RemotePaymentFactory.generateRemotePaymentItem
import cl.jaa.projects.payment.factory.RemotePaymentFactory.generateRemotePaymentMethods
import cl.jaa.projects.payment.data.remote.model.RemotePaymentItem
import cl.jaa.projects.payment.data.source.PaymentCache
import cl.jaa.projects.payment.data.source.PaymentRemote
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import io.mockk.InternalPlatformDsl.toStr
import io.mockk.coEvery
import io.mockk.coJustRun
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertTrue
import org.junit.Test

class PaymentRepositoryTest {

    private val remote = mockk<PaymentRemote>()
    private val cache = mockk<PaymentCache>()

    private val paymentRepository = PaymentRepository(
        remote, cache
    )
    private val gson: Gson = GsonBuilder().create()

    @Test
    fun `given remotePaymentMethods, when getPaymentMethods, then return data`() = runBlocking {
        val remotePaymentMethods = generateRemotePaymentMethods(2)
        stubGetRemotePaymentMethods(remotePaymentMethods)

        val data = paymentRepository.getPaymentMethods().first()

        assertTrue(data == remotePaymentMethods)
    }

    @Test
    fun `given paymentParams, when getPaymentParams, then return data`() = runBlocking {
        val cachePaymentParams = generateCachePaymentParams()
        stubGetAmount(cachePaymentParams)
        stubGetCard(cachePaymentParams)
        stubGetBank(cachePaymentParams)
        stubGetInstallments(cachePaymentParams)

        val data = paymentRepository.getPaymentParams().first()

        assertTrue(data == cachePaymentParams)
    }

    @Test
    fun `given amount, when saveAmount, then save on cache`() = runBlocking {
        val amount = Math.random().toStr()
        stubSaveAmount(amount)

        paymentRepository.saveAmount(amount)

        coVerify { cache.saveAmount(amount) }
    }

    @Test
    fun `given card, when saveCard, then save on cache`() = runBlocking {
        val card = generateRemotePaymentItem()
        val jsonCard = gson.toJson(card)
        stubSaveCard(jsonCard)

        paymentRepository.saveCard(card)

        coVerify { cache.saveCard(jsonCard) }
    }

    @Test
    fun `given bank, when saveBank, then save on cache`() = runBlocking {
        val bank = generateCardIssuersItem()
        val jsonBank = gson.toJson(bank)
        stubSaveBank(jsonBank)

        paymentRepository.saveBank(bank)

        coVerify { cache.saveBank(jsonBank) }
    }

    @Test
    fun `given installments, when saveInstallments, then save on cache`() = runBlocking {
        val installments = "3 cuotas"
        stubSaveInstallments(installments)

        paymentRepository.saveInstallments(installments)

        coVerify { cache.saveInstallments(installments) }
    }

    private fun stubSaveAmount(amount: String) {
        coJustRun { cache.saveAmount(amount) }
    }

    private fun stubSaveCard(card: String) {
        coJustRun { cache.saveCard(card) }
    }

    private fun stubSaveBank(bank: String) {
        coJustRun { cache.saveBank(bank) }
    }

    private fun stubSaveInstallments(installments: String) {
        coJustRun { cache.saveInstallments(installments) }
    }

    private fun stubGetRemotePaymentMethods(remotePaymentMethods: List<RemotePaymentItem>) {
        coEvery { remote.getRemotePaymentMethods() } returns remotePaymentMethods
    }

    private fun stubGetAmount(cachePaymentParams: CachePaymentParams) {
        coEvery { cache.getAmount() } returns flow { emit(cachePaymentParams.amount.toString()) }
    }

    private fun stubGetCard(cachePaymentParams: CachePaymentParams) {
        coEvery { cache.getCard() } returns flow { emit(gson.toJson(cachePaymentParams.card)) }
    }

    private fun stubGetBank(cachePaymentParams: CachePaymentParams) {
        coEvery { cache.getBank() } returns flow { emit(gson.toJson(cachePaymentParams.bank)) }
    }

    private fun stubGetInstallments(cachePaymentParams: CachePaymentParams) {
        coEvery { cache.getInstallments() } returns flow { emit(cachePaymentParams.installments.toString()) }
    }

}