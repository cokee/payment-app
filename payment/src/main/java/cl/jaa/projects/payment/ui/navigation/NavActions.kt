package cl.jaa.projects.payment.ui.navigation

import androidx.navigation.NavHostController

class NavActions(navHostController: NavHostController) {

    val entryPoint: () -> Unit = {
        navHostController.navigate(Routes.EntryPoint.path)
    }

    val payment: () -> Unit = {
        navHostController.navigate(Routes.Payment.path)
    }

    val bank: () -> Unit = {
        navHostController.navigate(Routes.Bank.path)
    }

    val installments: () -> Unit = {
        navHostController.navigate(Routes.Installments.path)
    }

    val receipt: () -> Unit = {
        navHostController.navigate(Routes.Receipt.path)
    }

    val upPress: () -> Unit = {
        navHostController.navigateUp()
    }

    val popBackStack: () -> Unit = {
        navHostController.popBackStack()
    }
}
