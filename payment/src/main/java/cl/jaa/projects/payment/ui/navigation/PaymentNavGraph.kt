package cl.jaa.projects.payment.ui.navigation

import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.compose.NavHost
import cl.jaa.projects.payment.presentation.entrypoint.EntryPointViewModel
import cl.jaa.projects.payment.presentation.selectbank.SelectBankViewModel
import cl.jaa.projects.payment.presentation.selectinstallment.SelectInstallmentViewModel
import cl.jaa.projects.payment.presentation.selectpayment.SelectPaymentViewModel
import cl.jaa.projects.payment.ui.di.PaymentComponentProvider
import com.google.accompanist.navigation.animation.rememberAnimatedNavController
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview

@ExperimentalMaterialApi
@FlowPreview
@ExperimentalCoroutinesApi
@ExperimentalAnimationApi
@Composable
fun PaymentNavGraph(
    fragmentActivity: FragmentActivity,
    startDestination: String = Routes.EntryPoint.path
) {
    val navController = rememberAnimatedNavController()
    val navActions = remember(navController) { NavActions(navController) }
    val component =
        (fragmentActivity.applicationContext as PaymentComponentProvider)
            .providePaymentSubcomponent()

    val coroutine = rememberCoroutineScope()
    val viewModelFactory = component.getViewModelFactory()

    val entryPointIntentHandler = component.getEntryPointIntentHandler().apply {
        coroutineScope = coroutine
    }

    val entryPointViewModel: EntryPointViewModel = viewModel<EntryPointViewModel>(
        factory = viewModelFactory
    ).apply {
        this.navActions = navActions
    }

    val paymentViewModel: SelectPaymentViewModel = viewModel<SelectPaymentViewModel>(
        factory = viewModelFactory
    ).apply {
        this.navActions = navActions
    }

    val paymentIntentHandler = component.getSelectPaymentIntentHandler().apply {
        coroutineScope = coroutine
    }

    val bankViewModel: SelectBankViewModel = viewModel<SelectBankViewModel>(
        factory = viewModelFactory
    ).apply {
        this.navActions = navActions
    }

    val bankIntentHandler = component.getSelectBankIntentHandler().apply {
        coroutineScope = coroutine
    }

    val installmentViewModel: SelectInstallmentViewModel = viewModel<SelectInstallmentViewModel>(
        factory = viewModelFactory
    ).apply {
        this.navActions = navActions
    }

    val installmentIntentHandler = component.getSelectInstallmentIntentHandler().apply {
        coroutineScope = coroutine
    }


    NavHost(
        navController = navController,
        startDestination = startDestination
    ) {
        entryPointNav(
            viewModel = entryPointViewModel,
            intentHandler = entryPointIntentHandler
        )
        paymentNav(
            viewModel = paymentViewModel,
            intentHandler = paymentIntentHandler
        )
        bankNav(
            viewModel = bankViewModel,
            intentHandler = bankIntentHandler
        )
        installmentNav(
            viewModel = installmentViewModel,
            intentHandler = installmentIntentHandler,
            actions = navActions
        )
    }
}
