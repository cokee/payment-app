package cl.jaa.projects.payment.presentation.selectpayment

import cl.jaa.projects.core.mvi.MviActionProcessor

interface SelectPaymentActionProcessor<A : SelectPaymentAction, B : SelectPaymentResult> :
    MviActionProcessor<A, B>