package cl.jaa.projects.payment.data.remote.model


import cl.jaa.projects.payment.data.remote.model.Constants.PAYER_COSTS
import com.google.gson.annotations.SerializedName

data class RemoteInstallmentsItem(
    @SerializedName(PAYER_COSTS)
    val payerCosts: List<RemotePayerCost>?
)