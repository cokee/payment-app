package cl.jaa.projects.payment.data.cache

import cl.jaa.projects.payment.data.remote.model.RemoteCardIssuersItem
import cl.jaa.projects.payment.data.remote.model.RemotePaymentItem

data class CachePaymentParams(
    val amount: String?,
    val card: RemotePaymentItem?,
    val bank: RemoteCardIssuersItem?,
    val installments: String?
)

