package cl.jaa.projects.payment.ui.navigation

sealed class Routes(val path: String) {
    object EntryPoint : Routes(path = "EntryPoint")
    object Payment : Routes(path = "Payment")
    object Bank : Routes(path = "Bank")
    object Installments : Routes(path = "installments")
    object Receipt : Routes(path = "receipt")
}