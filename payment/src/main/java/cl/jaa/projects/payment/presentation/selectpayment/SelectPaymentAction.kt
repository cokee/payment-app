package cl.jaa.projects.payment.presentation.selectpayment

import cl.jaa.projects.core.mvi.MviAction
import cl.jaa.projects.payment.ui.commons.CardItem

sealed class SelectPaymentAction : MviAction {

    object GetPaymentMethodsAction : SelectPaymentAction()
    data class SaveSelectedPaymentMethodAction(val selectedPaymentMethod: CardItem) :
        SelectPaymentAction()
}
