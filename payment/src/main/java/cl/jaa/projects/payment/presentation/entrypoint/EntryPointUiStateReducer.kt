package cl.jaa.projects.payment.presentation.entrypoint

import cl.jaa.projects.core.mvi.MviReducer

interface EntryPointUiStateReducer: MviReducer<EntryPointUiState, EntryPointResult>