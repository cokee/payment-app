package cl.jaa.projects.payment.presentation.entrypoint

import javax.inject.Inject
import javax.inject.Provider

class EntryPointReducer @Inject constructor(
    private val reducers: Map<Class<out EntryPointUiState>, @JvmSuppressWildcards Provider<EntryPointUiStateReducer>>
) :
    EntryPointUiStateReducer {
    override fun reduce(
        currentState: EntryPointUiState,
        result: EntryPointResult
    ): EntryPointUiState {
        return reducers[currentState::class.java]?.get()?.reduce(currentState, result)
            ?: EntryPointUiState.DefaultUiState
    }
}
