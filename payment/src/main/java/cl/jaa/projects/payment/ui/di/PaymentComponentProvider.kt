package cl.jaa.projects.payment.ui.di

import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview

@ExperimentalCoroutinesApi
@FlowPreview
interface PaymentComponentProvider {
    fun providePaymentSubcomponent(): PaymentSubcomponent
}
