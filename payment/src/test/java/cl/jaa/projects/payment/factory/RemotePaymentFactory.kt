package cl.jaa.projects.payment.factory

import cl.jaa.projects.payment.common.DefaultValues.BLANK_STRING
import cl.jaa.projects.payment.common.DefaultValues.ZERO_INT
import cl.jaa.projects.payment.data.remote.model.RemoteCardIssuersItem
import cl.jaa.projects.payment.data.remote.model.RemoteInstallmentsItem
import cl.jaa.projects.payment.data.remote.model.RemotePayerCost
import cl.jaa.projects.payment.data.remote.model.RemotePaymentItem

object RemotePaymentFactory {

    fun generateRemotePaymentMethods(count: Int = 2) = (0..count).map {
        generateRemotePaymentItem()
    }

    fun generateRemotePaymentItem() = RemotePaymentItem(
        id = BLANK_STRING,
        name = BLANK_STRING,
        thumbnail = BLANK_STRING
    )

    fun generateListCardIssuerItem(count: Int = 2) = (0..count).map {
        generateCardIssuersItem()
    }

    fun generateCardIssuersItem() = RemoteCardIssuersItem(
        id = BLANK_STRING,
        name = BLANK_STRING,
        thumbnail = BLANK_STRING
    )

    fun generateListRemoteInstallmentsItem(count: Int = 2) = (0..count).map {
        generateRemoteInstallmentsItem()
    }

    fun generateRemoteInstallmentsItem() = RemoteInstallmentsItem(
        payerCosts = generateListPayerCost()
    )

    private fun generateListPayerCost(count: Int = 2): List<RemotePayerCost> = (0..count).map {
        generateRemotePayerCost()
    }

    private fun generateRemotePayerCost() = RemotePayerCost(
        installments = ZERO_INT,
        recommendedMessage = BLANK_STRING
    )


}