package cl.jaa.projects.payment.presentation.selectpayment

import cl.jaa.projects.core.mvi.MviUiState
import cl.jaa.projects.payment.ui.commons.CardItem

sealed class SelectPaymentUiState : MviUiState {
    object DefaultUiState : SelectPaymentUiState()
    object LoadingUiState : SelectPaymentUiState()
    data class DisplayPaymentMethodsUiState(val cardItems: List<CardItem>) : SelectPaymentUiState()
    object ErrorUiState : SelectPaymentUiState()
}
