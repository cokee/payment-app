package cl.jaa.projects.payment.ui.di

import cl.jaa.projects.payment.presentation.selectbank.SelectBankUiState
import cl.jaa.projects.payment.presentation.selectbank.SelectBankUiStateReducer
import cl.jaa.projects.payment.presentation.selectbank.reducer.DefaultUiStateReducer
import cl.jaa.projects.payment.presentation.selectbank.reducer.DisplayBanksUiStateReducer
import cl.jaa.projects.payment.presentation.selectbank.reducer.ErrorUiStateReducer
import cl.jaa.projects.payment.presentation.selectbank.reducer.LoadingUiStateReducer
import dagger.Binds
import dagger.MapKey
import dagger.Module
import dagger.multibindings.IntoMap
import kotlin.reflect.KClass

@Module
abstract class SelectBankUiStateReducerModule {
    @Binds
    @IntoMap
    @SelectBankUiStateReducerKey(SelectBankUiState.DefaultUiState::class)
    abstract fun bindDefaultUiStateReducer(reducer: DefaultUiStateReducer): SelectBankUiStateReducer

    @Binds
    @IntoMap
    @SelectBankUiStateReducerKey(SelectBankUiState.LoadingUiState::class)
    abstract fun bindLoadingUiStateReducer(reducer: LoadingUiStateReducer): SelectBankUiStateReducer

    @Binds
    @IntoMap
    @SelectBankUiStateReducerKey(SelectBankUiState.DisplayBanksUiState::class)
    abstract fun bindDisplayBanksUiStateReducer(reducer: DisplayBanksUiStateReducer): SelectBankUiStateReducer

    @Binds
    @IntoMap
    @SelectBankUiStateReducerKey(SelectBankUiState.ErrorUiState::class)
    abstract fun bindErrorUiStateReducer(reducer: ErrorUiStateReducer): SelectBankUiStateReducer

}

@Retention(AnnotationRetention.RUNTIME)
@Target(
    AnnotationTarget.FUNCTION,
    AnnotationTarget.PROPERTY_GETTER,
    AnnotationTarget.PROPERTY_SETTER
)
@MapKey
annotation class SelectBankUiStateReducerKey(val kclass: KClass<out SelectBankUiState>)