package cl.jaa.projects.payment.presentation.selectpayment

import cl.jaa.projects.core.mvi.MviUserIntent
import cl.jaa.projects.payment.ui.commons.CardItem

sealed class SelectPaymentUIntent : MviUserIntent {
    data class SelectedPaymentMethodUIntent(val selectedPaymentId: CardItem) : SelectPaymentUIntent()
    object SeePaymentMethodsUIntent : SelectPaymentUIntent()
    object RetrySeeUIntent : SelectPaymentUIntent()
}
