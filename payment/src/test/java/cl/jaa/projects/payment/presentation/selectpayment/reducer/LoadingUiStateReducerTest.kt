package cl.jaa.projects.payment.presentation.selectpayment.reducer

import cl.jaa.projects.payment.factory.PaymentFactory.generateListCardItem
import cl.jaa.projects.payment.presentation.selectpayment.SelectPaymentResult
import cl.jaa.projects.payment.presentation.selectpayment.SelectPaymentUiState
import org.junit.Assert.assertTrue
import org.junit.Test

class LoadingUiStateReducerTest {
    private val reducer = LoadingUiStateReducer()

    @Test
    fun `given LoadingUiState with Success, when reduce, then return DisplayPaymentMethodsUiState`() {
        val currentState = SelectPaymentUiState.LoadingUiState
        val result = SelectPaymentResult.GetPaymentMethodsResult.Success(generateListCardItem())

        val newState = reducer.reduce(currentState, result)

        assertTrue(newState is SelectPaymentUiState.DisplayPaymentMethodsUiState)
    }

    @Test
    fun `given LoadingUiState with Failure, when reduce, then return ErrorUiState`() {
        val currentState = SelectPaymentUiState.LoadingUiState
        val result = SelectPaymentResult.GetPaymentMethodsResult.Failure

        val newState = reducer.reduce(currentState, result)

        assertTrue(newState is SelectPaymentUiState.ErrorUiState)
    }
}