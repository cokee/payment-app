package cl.jaa.projects.payment.presentation.selectinstallment

import cl.jaa.projects.core.mvi.MviUiState
import cl.jaa.projects.payment.presentation.selectinstallment.model.PaymentParams
import cl.jaa.projects.payment.ui.commons.SpinnerItem

sealed class SelectInstallmentUiState : MviUiState {
    object DefaultUiState : SelectInstallmentUiState()
    object LoadingUiState : SelectInstallmentUiState()
    data class DisplayInstallmentsUiState(val installments: List<SpinnerItem>) : SelectInstallmentUiState()
    data class DisplayPaymentParamsUiState(val paymentParams: PaymentParams) : SelectInstallmentUiState()
    object ErrorUiState : SelectInstallmentUiState()
}
