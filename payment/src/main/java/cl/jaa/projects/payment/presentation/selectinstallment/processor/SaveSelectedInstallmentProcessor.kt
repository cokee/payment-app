package cl.jaa.projects.payment.presentation.selectinstallment.processor

import cl.jaa.projects.payment.data.PaymentRepository
import cl.jaa.projects.payment.presentation.selectinstallment.SelectInstallmentAction
import cl.jaa.projects.payment.presentation.selectinstallment.SelectInstallmentActionProcessor
import cl.jaa.projects.payment.presentation.selectinstallment.SelectInstallmentResult
import cl.jaa.projects.payment.presentation.selectinstallment.mapper.InstallmentsMapper
import cl.jaa.projects.payment.presentation.selectinstallment.model.PaymentParams
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class SaveSelectedInstallmentProcessor @Inject constructor(
    private val repository: PaymentRepository
) : SelectInstallmentActionProcessor<SelectInstallmentAction, SelectInstallmentResult> {
    override suspend fun process(action: SelectInstallmentAction): Flow<SelectInstallmentResult> =
        flow {
            val actionResult = action as SelectInstallmentAction.SaveSelectedInstallmentAction
            repository.saveInstallments(actionResult.item.title)
            val params = repository.getPaymentParams().first()
            emit(
                SelectInstallmentResult.SaveInstallmentSelectedResult.Success(
                    PaymentParams(
                        amount = params.amount.toString(),
                        card = params.card?.name.toString(),
                        bank = params.bank?.name.toString(),
                        installments = params.installments.toString()
                    )
                )
            )
        }
}