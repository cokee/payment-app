package cl.jaa.projects.paymentapp.di

import cl.jaa.projects.payment.ui.di.PaymentSubcomponent
import dagger.Module
import kotlinx.coroutines.FlowPreview

@FlowPreview
@Module(subcomponents = [PaymentSubcomponent::class])
class SubcomponentsModule
