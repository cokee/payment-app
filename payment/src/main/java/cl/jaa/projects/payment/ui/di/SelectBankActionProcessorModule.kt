package cl.jaa.projects.payment.ui.di

import cl.jaa.projects.payment.presentation.selectbank.SelectBankAction
import cl.jaa.projects.payment.presentation.selectbank.SelectBankActionProcessor
import cl.jaa.projects.payment.presentation.selectbank.SelectBankResult
import cl.jaa.projects.payment.presentation.selectbank.processor.GetBanksProcessor
import cl.jaa.projects.payment.presentation.selectbank.processor.SaveSelectedBankProcessor
import dagger.Binds
import dagger.MapKey
import dagger.Module
import dagger.multibindings.IntoMap
import kotlin.reflect.KClass

@Module
abstract class SelectBankActionProcessorModule {

    @Binds
    @IntoMap
    @SelectBankActionProcessorKey(SelectBankAction.GetBanksAction::class)
    abstract fun bindGetBankProcessor(processor: GetBanksProcessor):
            SelectBankActionProcessor<SelectBankAction, SelectBankResult>

    @Binds
    @IntoMap
    @SelectBankActionProcessorKey(SelectBankAction.SaveSelectedGetBanksAction::class)
    abstract fun bindSaveSelectedBankProcessor(processor: SaveSelectedBankProcessor):
            SelectBankActionProcessor<SelectBankAction, SelectBankResult>

}

@MustBeDocumented
@Retention(AnnotationRetention.RUNTIME)
@Target(
    AnnotationTarget.FUNCTION,
    AnnotationTarget.PROPERTY_GETTER,
    AnnotationTarget.PROPERTY_SETTER
)
@MapKey
annotation class SelectBankActionProcessorKey(val kclass: KClass<out SelectBankAction>)