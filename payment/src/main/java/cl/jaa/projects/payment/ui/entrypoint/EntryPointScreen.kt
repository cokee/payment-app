package cl.jaa.projects.payment.ui.entrypoint

import androidx.compose.material.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import cl.jaa.projects.payment.presentation.entrypoint.EntryPointUiState
import cl.jaa.projects.payment.ui.commons.ErrorComponent

@Composable
fun EntryPointScreen(
    saveAmountEvent: (amount: String) -> Unit,
    uiState: State<EntryPointUiState>,
) {
    Scaffold {
        EntryPointContent(
            saveAmountEvent = saveAmountEvent,
            uiState = uiState
        )
    }
}

@Composable
fun EntryPointContent(
    saveAmountEvent: (amount: String) -> Unit,
    uiState: State<EntryPointUiState>
) {
    when (uiState.value) {
        EntryPointUiState.DefaultUiState -> DisplayInputAmountSection(saveAmountEvent)
        EntryPointUiState.ErrorUiState -> ErrorComponent()
    }
}
