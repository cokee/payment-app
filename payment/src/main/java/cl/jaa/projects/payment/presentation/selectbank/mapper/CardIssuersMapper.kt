package cl.jaa.projects.payment.presentation.selectbank.mapper

import cl.jaa.projects.payment.data.remote.model.RemoteCardIssuersItem
import cl.jaa.projects.payment.ui.commons.CardItem
import javax.inject.Inject

class CardIssuersMapper @Inject constructor() {

    fun List<RemoteCardIssuersItem>.toPresentation(): List<CardItem> = map { remote ->
        remote.toPresentation()
    }

    private fun RemoteCardIssuersItem.toPresentation() = CardItem(
        id = id.orEmpty(),
        title = name.orEmpty(),
        thumbnail = thumbnail.orEmpty()
    )

    fun CardItem.toRemote() = RemoteCardIssuersItem(
        id = id,
        name = title,
        thumbnail = thumbnail
    )
}