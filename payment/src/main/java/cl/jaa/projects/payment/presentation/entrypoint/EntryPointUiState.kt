package cl.jaa.projects.payment.presentation.entrypoint

import cl.jaa.projects.core.mvi.MviUiState

sealed class EntryPointUiState : MviUiState {

    object DefaultUiState : EntryPointUiState()
    object ErrorUiState: EntryPointUiState()
}
