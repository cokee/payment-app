package cl.jaa.projects.payment.ui.di

import cl.jaa.projects.payment.presentation.selectpayment.SelectPaymentAction
import cl.jaa.projects.payment.presentation.selectpayment.SelectPaymentAction.GetPaymentMethodsAction
import cl.jaa.projects.payment.presentation.selectpayment.SelectPaymentAction.SaveSelectedPaymentMethodAction
import cl.jaa.projects.payment.presentation.selectpayment.SelectPaymentActionProcessor
import cl.jaa.projects.payment.presentation.selectpayment.SelectPaymentResult
import cl.jaa.projects.payment.presentation.selectpayment.processor.GetPaymentMethodProcessor
import cl.jaa.projects.payment.presentation.selectpayment.processor.SaveSelectedPaymentMethodProcessor
import dagger.Binds
import dagger.MapKey
import dagger.Module
import dagger.multibindings.IntoMap
import kotlin.reflect.KClass

@Module
abstract class SelectPaymentActionProcessorModule {

    @Binds
    @IntoMap
    @SelectPaymentActionProcessorKey(GetPaymentMethodsAction::class)
    abstract fun bindGetPaymentMethodProcessor(processor: GetPaymentMethodProcessor): SelectPaymentActionProcessor<SelectPaymentAction, SelectPaymentResult>

    @Binds
    @IntoMap
    @SelectPaymentActionProcessorKey(SaveSelectedPaymentMethodAction::class)
    abstract fun bindSaveSelectedPaymentMethodProcessor(processor: SaveSelectedPaymentMethodProcessor): SelectPaymentActionProcessor<SelectPaymentAction, SelectPaymentResult>
}

@MustBeDocumented
@Retention(AnnotationRetention.RUNTIME)
@Target(
    AnnotationTarget.FUNCTION,
    AnnotationTarget.PROPERTY_GETTER,
    AnnotationTarget.PROPERTY_SETTER
)
@MapKey
annotation class SelectPaymentActionProcessorKey(val kclass: KClass<out SelectPaymentAction>)