package cl.jaa.projects.payment.presentation.selectinstallment

import javax.inject.Inject
import javax.inject.Provider

class SelectInstallmentReducer @Inject constructor(
    private val reducers: Map<Class<out SelectInstallmentUiState>, @JvmSuppressWildcards Provider<SelectInstallmentUiStateReducer>>
) :
    SelectInstallmentUiStateReducer {
    override fun reduce(
        currentState: SelectInstallmentUiState,
        result: SelectInstallmentResult
    ): SelectInstallmentUiState {
        return reducers[currentState::class.java]?.get()?.reduce(currentState, result)
            ?: SelectInstallmentUiState.DefaultUiState
    }
}
