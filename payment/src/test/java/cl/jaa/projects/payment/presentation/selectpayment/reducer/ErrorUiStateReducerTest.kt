package cl.jaa.projects.payment.presentation.selectpayment.reducer

import cl.jaa.projects.payment.presentation.selectpayment.SelectPaymentResult
import cl.jaa.projects.payment.presentation.selectpayment.SelectPaymentUiState
import org.junit.Assert.assertTrue
import org.junit.Test

class ErrorUiStateReducerTest {
    private val reducer = ErrorUiStateReducer()

    @Test
    fun `given ErrorUiState with InProgress, when reduce, then return LoadingUiState`() {
        val currentState = SelectPaymentUiState.ErrorUiState
        val result = SelectPaymentResult.GetPaymentMethodsResult.InProgress

        val newState = reducer.reduce(currentState, result)

        assertTrue(newState is SelectPaymentUiState.LoadingUiState)
    }
}