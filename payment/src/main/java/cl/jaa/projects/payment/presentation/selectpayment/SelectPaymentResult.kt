package cl.jaa.projects.payment.presentation.selectpayment

import cl.jaa.projects.core.mvi.MviResult
import cl.jaa.projects.payment.ui.commons.CardItem

sealed class SelectPaymentResult : MviResult {
    object NoneResult : SelectPaymentResult()

    sealed class GetPaymentMethodsResult : SelectPaymentResult() {
        object InProgress : GetPaymentMethodsResult()
        data class Success(val paymentMethods: List<CardItem>) : GetPaymentMethodsResult()
        object Failure : GetPaymentMethodsResult()
    }

    sealed class SaveSelectedPaymentMethodResult : SelectPaymentResult() {
        object Success : SaveSelectedPaymentMethodResult()
    }
}
