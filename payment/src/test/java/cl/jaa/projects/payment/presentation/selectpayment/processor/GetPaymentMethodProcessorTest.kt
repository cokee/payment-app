package cl.jaa.projects.payment.presentation.selectpayment.processor

import cl.jaa.projects.payment.data.PaymentRepository
import cl.jaa.projects.payment.data.remote.model.RemotePaymentItem
import cl.jaa.projects.payment.factory.PaymentFactory.generateListCardItem
import cl.jaa.projects.payment.factory.RemotePaymentFactory.generateRemotePaymentMethods
import cl.jaa.projects.payment.presentation.selectpayment.SelectPaymentAction
import cl.jaa.projects.payment.presentation.selectpayment.SelectPaymentResult
import cl.jaa.projects.payment.presentation.selectpayment.mapper.PaymentMethodsMapper
import cl.jaa.projects.payment.ui.commons.CardItem
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertTrue
import org.junit.Test

class GetPaymentMethodProcessorTest {
    private val repository = mockk<PaymentRepository>()
    private val mapper = mockk<PaymentMethodsMapper>()

    private val processor = GetPaymentMethodProcessor(
        repository, mapper
    )

    @Test
    fun `given GetPaymentMethodsAction, when processor, then result Success with data`() =
        runBlocking {
            val remotePaymentMethods = generateRemotePaymentMethods()
            val paymentMethods = generateListCardItem()
            stubGetPaymentMethods(remotePaymentMethods)
            stubPaymentMapper(remotePaymentMethods, paymentMethods)

            val stateFlow = processor.process(SelectPaymentAction.GetPaymentMethodsAction).toList()

            assertTrue(stateFlow.first() is SelectPaymentResult.GetPaymentMethodsResult.InProgress)
            assertTrue(stateFlow.last() is SelectPaymentResult.GetPaymentMethodsResult.Success)
            assertTrue((stateFlow.last() as SelectPaymentResult.GetPaymentMethodsResult.Success).paymentMethods == paymentMethods)
        }

    @Test
    fun `given GetPaymentMethodsAction, when processor has an error, then result Failure`() =
        runBlocking {
            stubGetPaymentMethodsError()

            val stateFlow = processor.process(SelectPaymentAction.GetPaymentMethodsAction).toList()

            assertTrue(stateFlow.first() is SelectPaymentResult.GetPaymentMethodsResult.InProgress)
            assertTrue(stateFlow.last() is SelectPaymentResult.GetPaymentMethodsResult.Failure)
        }

    private fun stubGetPaymentMethods(remotePaymentMethods: List<RemotePaymentItem>) {
        coEvery { repository.getPaymentMethods() } returns flow { emit(remotePaymentMethods) }
    }

    private fun stubGetPaymentMethodsError() {
        coEvery { repository.getPaymentMethods() } returns flow { throw Throwable("") }
    }

    private fun stubPaymentMapper(
        remotePaymentMethods: List<RemotePaymentItem>,
        paymentMethods: List<CardItem>
    ) {
        coEvery { with(mapper) { remotePaymentMethods.toPresentation() } } returns paymentMethods
    }
}