package cl.jaa.projects.payment.presentation.selectbank

import cl.jaa.projects.core.mvi.MviUiState
import cl.jaa.projects.payment.ui.commons.CardItem

sealed class SelectBankUiState : MviUiState {
    object DefaultUiState : SelectBankUiState()
    object LoadingUiState : SelectBankUiState()
    data class DisplayBanksUiState(val banks: List<CardItem>) : SelectBankUiState()
    object ErrorUiState : SelectBankUiState()
}
