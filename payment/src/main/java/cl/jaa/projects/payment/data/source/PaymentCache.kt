package cl.jaa.projects.payment.data.source

import kotlinx.coroutines.flow.Flow

interface PaymentCache {
    suspend fun saveAmount(amount: String)
    suspend fun saveCard(card: String)
    suspend fun saveBank(bank: String)
    suspend fun saveInstallments(installments: String)
    suspend fun getAmount(): Flow<String>
    suspend fun getCard(): Flow<String>
    suspend fun getBank(): Flow<String>
    suspend fun getInstallments(): Flow<String>
}