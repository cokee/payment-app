package cl.jaa.projects.payment.ui.di

import cl.jaa.projects.payment.presentation.entrypoint.EntryPointAction
import cl.jaa.projects.payment.presentation.entrypoint.EntryPointActionProcessor
import cl.jaa.projects.payment.presentation.entrypoint.EntryPointResult
import cl.jaa.projects.payment.presentation.entrypoint.processor.SaveAmountActionProcessor
import dagger.Binds
import dagger.MapKey
import dagger.Module
import dagger.multibindings.IntoMap
import kotlin.reflect.KClass

@Module
abstract class EntryPointActionProcessorModule {

    @Binds
    @IntoMap
    @EntryPointActionProcessorKey(EntryPointAction.SaveAmountAction::class)
    abstract fun bindSaveAmountActionProcessor(processor: SaveAmountActionProcessor): EntryPointActionProcessor<EntryPointAction, EntryPointResult>
}

@MustBeDocumented
@Retention(AnnotationRetention.RUNTIME)
@Target(
    AnnotationTarget.FUNCTION,
    AnnotationTarget.PROPERTY_GETTER,
    AnnotationTarget.PROPERTY_SETTER
)
@MapKey
annotation class EntryPointActionProcessorKey(val kclass: KClass<out EntryPointAction>)