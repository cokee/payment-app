package cl.jaa.projects.payment.presentation.entrypoint

import cl.jaa.projects.core.mvi.MviAction

sealed class EntryPointAction : MviAction {

    data class SaveAmountAction(val amount: String) : EntryPointAction()
}
