package cl.jaa.projects.payment.presentation.selectbank

import cl.jaa.projects.core.mvi.MviAction
import cl.jaa.projects.payment.ui.commons.CardItem

sealed class SelectBankAction : MviAction {
    object GetBanksAction : SelectBankAction()
    data class SaveSelectedGetBanksAction(val item: CardItem): SelectBankAction()
}
