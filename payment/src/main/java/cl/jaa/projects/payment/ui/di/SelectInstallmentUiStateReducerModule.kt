package cl.jaa.projects.payment.ui.di

import cl.jaa.projects.payment.presentation.selectinstallment.SelectInstallmentUiState
import cl.jaa.projects.payment.presentation.selectinstallment.SelectInstallmentUiStateReducer
import cl.jaa.projects.payment.presentation.selectinstallment.reducer.*
import dagger.Binds
import dagger.MapKey
import dagger.Module
import dagger.multibindings.IntoMap
import kotlin.reflect.KClass

@Module
abstract class SelectInstallmentUiStateReducerModule {
    @Binds
    @IntoMap
    @SelectInstallmentUiStateReducerKey(SelectInstallmentUiState.DefaultUiState::class)
    abstract fun bindDefaultUiStateReducer(reducer: DefaultUiStateReducer): SelectInstallmentUiStateReducer

    @Binds
    @IntoMap
    @SelectInstallmentUiStateReducerKey(SelectInstallmentUiState.LoadingUiState::class)
    abstract fun bindLoadingUiStateReducer(reducer: LoadingUiStateReducer): SelectInstallmentUiStateReducer

    @Binds
    @IntoMap
    @SelectInstallmentUiStateReducerKey(SelectInstallmentUiState.DisplayInstallmentsUiState::class)
    abstract fun bindDisplayInstallmentUiStateReducer(reducer: DisplayInstallmentUiStateReducer): SelectInstallmentUiStateReducer

    @Binds
    @IntoMap
    @SelectInstallmentUiStateReducerKey(SelectInstallmentUiState.ErrorUiState::class)
    abstract fun bindErrorUiStateReducer(reducer: ErrorUiStateReducer): SelectInstallmentUiStateReducer

    @Binds
    @IntoMap
    @SelectInstallmentUiStateReducerKey(SelectInstallmentUiState.DisplayPaymentParamsUiState::class)
    abstract fun bindDisplayPaymentParamsUiStateReducer(reducer: DisplayPaymentParamsUiStateReducer): SelectInstallmentUiStateReducer

}

@Retention(AnnotationRetention.RUNTIME)
@Target(
    AnnotationTarget.FUNCTION,
    AnnotationTarget.PROPERTY_GETTER,
    AnnotationTarget.PROPERTY_SETTER
)
@MapKey
annotation class SelectInstallmentUiStateReducerKey(val kclass: KClass<out SelectInstallmentUiState>)