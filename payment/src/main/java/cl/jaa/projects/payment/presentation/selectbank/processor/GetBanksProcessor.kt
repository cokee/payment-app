package cl.jaa.projects.payment.presentation.selectbank.processor

import cl.jaa.projects.payment.data.PaymentRepository
import cl.jaa.projects.payment.presentation.selectbank.SelectBankAction
import cl.jaa.projects.payment.presentation.selectbank.SelectBankActionProcessor
import cl.jaa.projects.payment.presentation.selectbank.SelectBankResult
import cl.jaa.projects.payment.presentation.selectbank.SelectBankResult.GetBanksResult.*
import cl.jaa.projects.payment.presentation.selectbank.mapper.CardIssuersMapper
import cl.jaa.projects.payment.presentation.selectinstallment.mapper.InstallmentsMapper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import javax.inject.Inject

class GetBanksProcessor @Inject constructor(
    private val repository: PaymentRepository,
    private val mapper: CardIssuersMapper
) : SelectBankActionProcessor<SelectBankAction, SelectBankResult> {
    override suspend fun process(action: SelectBankAction): Flow<SelectBankResult> {
        return repository.getCardIssuers().map { cardIssuers ->
            Success(
                with(mapper) { cardIssuers.toPresentation() }
            ) as SelectBankResult.GetBanksResult
        }.onStart {
            emit(InProgress)
        }.catch {
            emit(Failure)
        }.flowOn(Dispatchers.IO)
    }


}