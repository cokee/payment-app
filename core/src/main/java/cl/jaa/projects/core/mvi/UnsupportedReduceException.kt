package cl.jaa.projects.core.mvi

import java.lang.RuntimeException

class UnsupportedReduceException(state: MviUiState, result: MviResult) :
    RuntimeException("Error reduce state")