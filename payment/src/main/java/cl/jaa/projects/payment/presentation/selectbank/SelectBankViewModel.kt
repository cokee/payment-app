package cl.jaa.projects.payment.presentation.selectbank

import cl.jaa.projects.payment.presentation.selectbank.SelectBankUIntent.RetrySeeBanksUIntent
import cl.jaa.projects.payment.presentation.selectbank.SelectBankUIntent.SeeBanksUIntent
import cl.jaa.projects.payment.ui.commons.MviViewModel
import cl.jaa.projects.payment.ui.navigation.NavActions
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@FlowPreview
class SelectBankViewModel @Inject constructor(
    reducer: SelectBankReducer,
    processor: SelectBankProcessor
) : MviViewModel<SelectBankUiStateReducer,
        SelectBankUiState,
        SelectBankResult,
        SelectBankAction,
        SelectBankProcessor,
        SelectBankUIntent>(
    reducer,
    processor,
    SelectBankUiState.DefaultUiState
) {
    var navActions: NavActions? = null

    override fun SelectBankUIntent.toAction(): SelectBankAction {
        return when (this) {
            is SeeBanksUIntent, RetrySeeBanksUIntent -> SelectBankAction.GetBanksAction
            is SelectBankUIntent.SelectedBankUIntent -> SelectBankAction.SaveSelectedGetBanksAction(
                selectedBank
            )
        }
    }

    override fun Flow<SelectBankResult>.checkResultToNavigation(): Flow<SelectBankResult> =
        onEach { result ->
            when (result) {
                SelectBankResult.SaveBankSelectedResult.Success -> {
                    navActions?.installments?.invoke()
                }
                else -> return@onEach
            }
        }
}

