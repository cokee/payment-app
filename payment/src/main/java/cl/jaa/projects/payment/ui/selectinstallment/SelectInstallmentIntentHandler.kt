package cl.jaa.projects.payment.ui.selectinstallment

import cl.jaa.projects.payment.presentation.selectinstallment.SelectInstallmentUIntent
import cl.jaa.projects.payment.ui.commons.SpinnerItem
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

class SelectInstallmentIntentHandler @Inject constructor() {

    private val userIntents = MutableSharedFlow<SelectInstallmentUIntent>()
    var coroutineScope: CoroutineScope? = null

    fun userIntents(): Flow<SelectInstallmentUIntent> = merge(
        flow { emit(SelectInstallmentUIntent.SeeInstallmentUIntent) },
        userIntents.asSharedFlow()
    )

    fun saveInstallmentSelected(selectedInstallment: SpinnerItem) {
        coroutineScope?.launch {
            userIntents.emit(SelectInstallmentUIntent.SelectedInstallmentUIntent(selectedInstallment))
        }
    }
}
