package cl.jaa.projects.payment.presentation.selectpayment.reducer

import cl.jaa.projects.core.mvi.UnsupportedReduceException
import cl.jaa.projects.payment.presentation.selectpayment.SelectPaymentResult
import cl.jaa.projects.payment.presentation.selectpayment.SelectPaymentResult.GetPaymentMethodsResult.InProgress
import cl.jaa.projects.payment.presentation.selectpayment.SelectPaymentUiReducer
import cl.jaa.projects.payment.presentation.selectpayment.SelectPaymentUiState
import cl.jaa.projects.payment.presentation.selectpayment.SelectPaymentUiState.LoadingUiState
import javax.inject.Inject

class ErrorUiStateReducer @Inject constructor() : SelectPaymentUiReducer {
    override fun reduce(
        currentState: SelectPaymentUiState,
        result: SelectPaymentResult
    ): SelectPaymentUiState {
        return when (result) {
            is InProgress -> LoadingUiState
            else -> throw UnsupportedReduceException(currentState, result)
        }

    }

}