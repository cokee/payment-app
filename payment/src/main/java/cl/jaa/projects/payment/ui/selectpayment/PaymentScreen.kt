package cl.jaa.projects.payment.ui.selectpayment

import androidx.compose.material.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import androidx.compose.ui.res.stringResource
import cl.jaa.projects.payment.R
import cl.jaa.projects.payment.presentation.selectpayment.SelectPaymentUiState
import cl.jaa.projects.payment.ui.commons.*

@Composable
fun PaymentScreen(
    state: State<SelectPaymentUiState>,
    selectCardEvent: (cardItem: CardItem) -> Unit
) {
    Scaffold {
        PaymentContent(state = state, selectCardEvent = selectCardEvent)
    }
}

@Composable
private fun PaymentContent(
    state: State<SelectPaymentUiState>,
    selectCardEvent: (cardItem: CardItem) -> Unit
) {
    when (val uiState = state.value) {
        SelectPaymentUiState.DefaultUiState -> DefaultContent()
        is SelectPaymentUiState.DisplayPaymentMethodsUiState -> DisplaySelectableCardItems(
            cardItems = uiState.cardItems,
            selectCardEvent = selectCardEvent,
            headerLabel = stringResource(id = R.string.select_payment_method)
        )
        SelectPaymentUiState.ErrorUiState -> ErrorComponent()
        SelectPaymentUiState.LoadingUiState -> LoadingComponent()
    }
}