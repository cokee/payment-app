package cl.jaa.projects.payment.presentation.selectinstallment

import cl.jaa.projects.core.mvi.MviProcessor
import javax.inject.Inject
import javax.inject.Provider

class SelectInstallmentProcessor @Inject constructor(
    processors: Map<Class<out SelectInstallmentAction>, @JvmSuppressWildcards Provider<SelectInstallmentActionProcessor<SelectInstallmentAction, SelectInstallmentResult>>>
) : MviProcessor<SelectInstallmentAction, SelectInstallmentResult, SelectInstallmentActionProcessor<SelectInstallmentAction, SelectInstallmentResult>>(
    processors = processors,
    defaultResult = SelectInstallmentResult.NoneResult
)
